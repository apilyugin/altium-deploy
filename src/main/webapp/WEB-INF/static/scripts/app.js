function ProgressDialog(url,completeStatus,failureStatus,callback){

    var modal = new $.UIkit.modal.Modal("#progressModal");
    var progress = $("#progressModal").find(".uk-progress");
    var progressBar = progress.find(".uk-progress-bar");

    var startPercent = 10;


    var updateProgressBar = function(num){
        progressBar.width(num + "%");
        progressBar.text(num + "%");
    }

    var getInfo = function(){
        $.ajax({
            url: url
        }).done(function(responseText){
            var response;
            if(typeof responseText == "object"){
                response = responseText;
            } else {
                response = $.parseJSON(responseText);
            }
            if(response.success){
                var data = response.data[0];
                if((data.status == completeStatus) || (data.status == failureStatus)){
                    updateProgressBar(100);
                    setTimeout(function(){
                        modal.hide();
                        if(typeof callback == "function"){
                            callback();
                        }
                    },400);
                } else {
                    if(startPercent == 100){
                        startPercent = 10;
                    } else {
                        startPercent+=10
                    }
                    updateProgressBar(startPercent);
                    setTimeout(function(){
                        getInfo();
                    }, 1000);

                }
            }
        });
    }

    updateProgressBar(startPercent);
    modal.show();
    getInfo();

}

function MessageModalDialog(message){
    var modal = new $.UIkit.modal.Modal("#messageModal");
    modal.find(".message").html(message);
    modal.show();
}

$(document).ready(function(){

    var dumpRecords = new DumpRecords();
    var importRecords = new ImportedRecords();
    var uploadRecords = new UploadedRecords();
    var serverRecords = new ServerRecords();


    ko.applyBindings(dumpRecords,$("#dumpTable")[0]);
    ko.applyBindings(importRecords,$("#importContainer")[0]);
    ko.applyBindings(uploadRecords,$("#uploadContainer")[0]);
    ko.applyBindings(serverRecords,$("#serverList")[0]);


    var Routing = {
        allNavigation : function(){
            console.log(arguments);
            document.location.hash = "#/dumps";
        },
        dumps : function(){
            ChangeNavigation('dumps');
            dumpRecords.loadItems();

        },
        uploads : function(){
            ChangeNavigation('uploads');
            uploadRecords.loadItems();
        },
        imports : function(){
            ChangeNavigation('imports');
            importRecords.loadItems();
        },
        servers : function(){
            ChangeNavigation('servers');
            serverRecords.loadItems();
        }
    }

    var routes = {
        '/uploads': Routing.uploads,
        '/dumps': Routing.dumps,
        '/imports': Routing.imports,
        '/servers': Routing.servers,
        '/*': Routing.allNavigation

    };

    var router = Router(routes);
    router.init();

    var hash = document.location.hash;
    if(!hash || hash == ""){
        Routing.dumps();
    }




});

function ChangeNavigation(page){
    $(".uk-navbar li").removeClass("uk-active");
    $(".uk-navbar li[data-page='" + page + "']").addClass('uk-active');

    var $dataContent = $("#dataContent");
    $dataContent.find(".container").hide();
    $dataContent.find(".container[data-page='" + page + "']").show();
}


