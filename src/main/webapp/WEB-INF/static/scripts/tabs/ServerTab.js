function ServerRecords() {

    var me = this;
    var addServer = new AddServer(me);
    var removeServer = new RemoveServer(me);

    me.items = ko.observableArray([]);
    me.selectedRecord = ko.observable(null);

    me.loadItems = function(){
        $.ajax({
            url: "server",
            success: function (response) {
                if (!_.isObject(response)) {
                    response = $.parseJSON(response);
                }
                me.items(response.data);
            }
        });
    }


    ko.applyBindings(addServer,$("#serverAdd")[0]);
    ko.applyBindings(removeServer,$("#serverRemove")[0]);
    addServer.initModal();
    removeServer.initModal();


    me.openRemoveDialog = function(){
        removeServer.open();
    }

    me.openAddDialog = function(){
        addServer.open()
    }

    me.selectRecord = function(record,el){
        me.selectedRecord(record);
    }

    me.loadItems();

}

function AddServer(ServerList) {

    var me = this;

    var modal = null;

    me.name = ko.observable("");
    me.url = ko.observable("");
    me.port = ko.observable("");

    me.reset = function(){
        me.name("");
        me.url("");
        me.port("");
    }

    me.addServer = function(){
        $.ajax({
            url:"server",
            type : "POST",
            data : {
                name : me.name(),
                url : me.url(),
                port : me.port()
            }
        }).done(function(){
                me.reset();
                modal.hide();
                ServerList.loadItems();
            });
        return false;
    }

    me.initModal = function(){
        modal = new $.UIkit.modal.Modal("#serverModalAdd");
    }

    me.open = function(){
        modal.show();
    }

    me.hide = function(){
        modal.hide();
    }


}

function RemoveServer(ServerList){

    var me = this;
    var modal = null;

    me.remove = function(){
        $.ajax({
            url:"server/" + ServerList.selectedRecord().id,
            type : "DELETE"
        }).done(function(){
                me.hide();
                ServerList.loadItems();
            });
    }

    me.cancel = function(){
        me.hide();
    }

    me.initModal = function(){
        modal = new $.UIkit.modal.Modal("#serverModalRemove");
    }

    me.open = function(){
        if(ServerList.selectedRecord() != null){
            modal.show();
        }
        //console.log)

    }

    me.hide = function(){
        modal.hide();
    }


}