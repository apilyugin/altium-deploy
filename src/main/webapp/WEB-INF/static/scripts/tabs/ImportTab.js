function ImportedRecords(){
    var me = this;

    this.items = ko.observableArray([]);

    me.loadItems = function(){
        $.ajax({
            url: "import",
            success: function (response) {
                if (!_.isObject(response)) {
                    response = $.parseJSON(response);
                }
                me.items(response.data);
            }
        })
    }

    me.createImport = function(){

    }

}