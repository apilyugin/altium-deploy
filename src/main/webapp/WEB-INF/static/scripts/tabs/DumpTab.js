function DumpRecords(){
    var me = this;

    me.items = ko.observableArray([]);
    me.selectedRecord = ko.observable(null);
    var uploadView = new DumpUploadView(this);

    ko.applyBindings(uploadView,$("#dumpUpload")[0]);
    uploadView.initDialog();


    me.loadItems = function(){
        $.ajax({
            url : "dump",
            success : function(response){
                if(!_.isObject(response)){
                    response = $.parseJSON(response);
                }
                me.items(response.data);
            }
        });
    }


    var baseUrl = "/dump";

    var getDetailUrl = function(id){
        var url = baseUrl + "/{id}";
        return url.replace("{id}",id);
    }


    me.createDump = function(){
        $.ajax({
            url:baseUrl,
            type : "POST"
        }).done(function(responseText){
                var response;
                if(typeof responseText == "object"){
                    response = responseText;
                } else {
                    response = $.parseJSON(responseText);
                }
                if(response.success && response.data && response.data[0]){
                    var data = response.data[0];
                    ProgressDialog(getDetailUrl(data.id),DumpRecords.STATUS_COMPLETED,DumpRecords.STATUS_FAILURE,function(){
                        me.loadItems();
                    });
                }
            });
    }

    me.uploadDialog = function(){
        if(me.selectedRecord() != null){
            uploadView.openDialog();
        }
    }

}

DumpRecords.STATUS_COMPLETED = "completed";
DumpRecords.STATUS_FAILURE = "failure";

function DumpUploadView(DumpList){

    var me = this;
    var serverUrl = "/server";
    var dumpUrl = "/dump";
    var modal = null;

    me.availableServers = ko.observableArray([]);
    me.selectedServer = ko.observable(null);

    var getUploadUrl = function(dump_id,server_id){
        var url = dumpUrl + "/{dump_id}/server/{server_id}";
        url = url.replace("{dump_id}",dump_id);
        return url.replace("{server_id}",server_id);
    }


    me.loadServers = function(){
        $.ajax({
            url:serverUrl,
            type : "GET"
        }).done(function(responseText){
                var response;
                if (typeof responseText == "object"){
                    response = responseText;
                } else {
                    response = $.parseJSON(responseText);
                }
                if (response.success && response.data && response.data[0]) {
                    me.availableServers(response.data);
                    if(me.availableServers().length == 1){
                        me.selectedServer([me.availableServers()[0].id]);
                    }
                }
            });
    }

    me.initDialog = function(){
        modal = new $.UIkit.modal.Modal("#dumpModalUpload");
    }

    me.openDialog = function(){
        modal.show();
        me.loadServers();
    }

    me.hideDialog = function(){
        modal.hide();
    }

    me.uploadDump = function(){
        var server_id = me.selectedServer()[0];
        var dump_id = DumpList.selectedRecord();
        $.ajax({
            url: getUploadUrl(dump_id,server_id),
            type:"GET"
        }).done(function(responseText){
                var response;
                if(typeof responseText == "object"){
                    response = responseText;
                } else{
                    response = $.parseJSON(responseText)
                }
                if(response.success){
                    modal.hide();
                }
            });
        console.log("Upload Dump", server_id, dump_id);
    }
}