function UploadedRecords(){

    var me = this;
    var importUrl = "/import";

    var getImportUploadUrl = function(uploadId){
        var url = importUrl + "/upload/{uploadId}";
        return url.replace("{uploadId}",uploadId);
    }

    var getImportDetailUrl = function(id){
        var url = importUrl + "/{id}";
        return url.replace("{id}",id);
    }

    me.items = ko.observableArray([]);

    me.selectedRecord = ko.observable(null);

    me.loadItems = function(){
        $.ajax({
            url : "upload",
            success : function(response){
                if(!_.isObject(response)){
                    response = $.parseJSON(response);
                }
                me.items(response.data);
            }
        })
    }

    me.importUpload = function(){
        if(me.selectedRecord() == null){
            return;
        }
        $.ajax({
            url:getImportUploadUrl(me.selectedRecord()),
            type : "POST"
        }).done(function(responseText){
                var response;
                if(typeof responseText == "object"){
                    response = responseText;
                } else {
                    response = $.parseJSON(responseText);
                }
                if(response.success && response.data && response.data[0]){
                    var data = response.data[0];
                    ProgressDialog(getImportDetailUrl(data.id),UploadedRecords.STATUS_COMPLETED,UploadedRecords.STATUS_FAILURE,function(){
                        me.loadItems();
                    });
                }
            });
    }
}

UploadedRecords.STATUS_COMPLETED = "completed";
UploadedRecords.STATUS_FAILURE = "failure";