<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="/c/reset.css" />
    <link rel="stylesheet" type="text/css" href="/c/uikit.gradient.css" />
    <link rel="stylesheet" type="text/css" href="/c/main.css" />
    <script type="text/javascript" src="/s/lib/director.js"></script>
    <script type="text/javascript" src="/s/lib/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="/s/lib/underscore-1.5.2.js"></script>
    <script type="text/javascript" src="/s/lib/uikit.js"></script>
    <script type="text/javascript" src="/s/lib/knockoutjs-3.0.0.js"></script>
    <script type="text/javascript" src="/s/tabs/DumpTab.js"></script>
    <script type="text/javascript" src="/s/tabs/ImportTab.js"></script>
    <script type="text/javascript" src="/s/tabs/ServerTab.js"></script>
    <script type="text/javascript" src="/s/tabs/UploadTab.js"></script>
    <script type="text/javascript" src="/s/app.js"></script>

    <!-- Dump Tab Templates -->
    <script type="text/html" id="dumpTableTemplate">
        <table class="uk-table">

            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Status</th>
                <th>
                    <div class="uk-button-dropdown" data-uk-dropdown>
                        <button class="uk-button">Actions</button>
                        <!-- This is the dropdown -->
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav uk-nav-dropdown">
                                <li><a href="#" data-bind="click:createDump">Create</a></li>
                                <li><a href="#" data-bind="click:uploadDialog">Upload</a></li>
                            </ul>
                        </div>
                    </div>
                </th>
            </tr>
            <!-- ko foreach: items -->
            <tr>
                <td><input type="radio" name="selectedId" data-bind="checked:$parent.selectedRecord, value:id"></td>
                <td data-bind="text: createdOn"></td>
                <td data-bind="text: status"></td>
                <td></td>
            </tr>
            <!-- /ko -->
        </table>
    </script>

    <script type="text/html" id="dumpUploadTemplate">
        <div id="dumpModalUpload" class="uk-modal">
            <div class="uk-modal-dialog">
                <a class="uk-modal-close uk-close"></a>
                <table class="uk-table">
                    <tr>
                        <td>Server: </td>
                        <td>
                            <select data-bind="foreach:availableServers, selectedOptions:selectedServer">
                                <option data-bind="text:name,value:id"></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><button class="uk-button" data-bind="click:uploadDump">Upload</button></td>
                    </tr>
                </table>
            </div>
        </div>
    </script>

    <!-- end Dump Tab Templates -->

    <!-- Uploads Template -->

    <script type="text/html" id="uploadTable">
        <table class="uk-table">
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>
                    <div class="uk-button-dropdown" data-uk-dropdown>
                        <button class="uk-button">Actions</button>
                        <!-- This is the dropdown -->
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav uk-nav-dropdown">
                                <li><a href="#" data-bind="click:importUpload">Import</a></li>
                            </ul>
                        </div>
                    </div>
                    </td>
                </th>
            </tr>
            <!-- ko foreach: items -->
            <tr>
                <td><input type="radio" name="id" data-bind="value: id, checked:$parent.selectedRecord" /></td>
                <td data-bind="text: createdOn"></td>
                <td>

            </tr>
            <!-- /ko -->
        </table>
    </script>

    <!-- end Uploads Template -->

    <!-- Import Template -->
    <script type="text/html" id="importTable">
        <table class="uk-table">
            <tr>
                <th></th>
                <th>ID</th>
                <th>Status</th>
                <th>Upload Id</th>
                <th>Stared On</th>
                <th>Finished On</th>
            </tr>
            <!-- ko foreach: items -->
            <tr>
                <td><input type="radio" name="id" data-bind="value: id, check:selectedRecord" /></td>
                <td data-bind="text: id"></td>
                <td data-bind="text: status"></td>
                <td data-bind="text: uploadId"></td>
                <td data-bind="text: startedOn"></td>
                <td data-bind="text: finishedOn"></td>
            </tr>
            <!-- /ko -->
        </table>
    </script>

    <!-- end Import Template -->

    <!-- Server Tab Templates -->

    <script type="text/html" id="serverTable">
        <table class="uk-table">
            <tr>
                <th></th>
                <th>ID</th>
                <th>Name</th>
                <th>Url</th>
                <th>Port</th>
                <th>
                    <div class="uk-button-dropdown" data-uk-dropdown>


                        <button class="uk-button">Actions</button>

                        <!-- This is the dropdown -->
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav uk-nav-dropdown">
                                <li><a href="#" data-bind="click:openAddDialog">Add</a></li>
                                <li><a href="#" data-bind="click:openRemoveDialog">Remove</a></li>
                            </ul>
                        </div>

                    </div>

                </th>
            </tr>
            <!-- ko foreach: items -->
            <tr>
                <td><input type="radio" name="id" data-bind="value: id, click:$parent.selectRecord" /></td>
                <td data-bind="text: id"></td>
                <td data-bind="text: name"></td>
                <td data-bind="text: url"></td>
                <td data-bind="text: port"></td>
                <td>

                </td>
            </tr>
            <!-- /ko -->
        </table>

    </script>

    <script type="text/html" id="serverModalAddTemplate">
        <!-- This is the modal -->
        <div id="serverModalAdd" class="uk-modal">
            <div class="uk-modal-dialog">
                <a class="uk-modal-close uk-close"></a>
                <table class="uk-table">
                    <tr>
                        <td>Name: </td>
                        <td><input type="text" data-bind="value: name"></td>
                    </tr>
                    <tr>
                        <td>Url: </td>
                        <td><input type="text" data-bind="value: url"></td>
                    </tr>
                    <tr>
                        <td>Port: </td>
                        <td><input type="text" data-bind="value: port"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><button class="uk-button" data-bind="click:addServer">Add</button></td>
                    </tr>
                </table>
            </div>
        </div>

    </script>

    <script type="text/html" id="serverModalRemoveTemplate">
        <div id="serverModalRemove" class="uk-modal">
            <div class="uk-modal-dialog">
                <a class="uk-modal-close uk-close"></a>
                <h3>Are you sure?</h3>
                <div>
                    <button class="uk-button" data-bind="click:remove">Remove</button>
                    <button class="uk-button" data-bind='click:cancel'>Cancel</button>
                </div>
            </div>
        </div>
    </script>

    <!-- End server Tab Templates -->

    <style type="text/css">
        #dataContent div.container{
            display: none;
        }

        #dataContent div.container.uk-active{
            display: block;
        }
    </style>

</head>
<body>

<div class="uk-panel">


        <div class="uk-navbar">
            <ul class="uk-navbar-nav">
                <li data-page="dumps" class="uk-active"><a href="#/dumps">Dumps</a></li>
                <li data-page="uploads"><a href="#/uploads">Uploads</a></li>
                <li data-page="imports"><a href="#/imports">Imports</a></li>
                <li data-page="servers"><a href="#/servers">Servers</a></li>
            </ul>
        </div>


    <div id="dataContent">

        <div id="dumpContainer" class="container" data-page="dumps">
            <div id="dumpTable" data-bind="template: { name: 'dumpTableTemplate'}"></div>
            <div id="dumpUpload" data-bind="template: { name: 'dumpUploadTemplate'}"></div>
        </div>

        <div data-bind="template: { name: 'uploadTable'}" id="uploadContainer" class="container" data-page="uploads"></div>

        <div data-bind="template: { name: 'importTable'}" id="importContainer" class="container" data-page="imports"></div>

        <div class="container" data-page="servers">
            <div data-bind="template: { name: 'serverTable'}" id="serverList"></div>
            <div data-bind="template: { name: 'serverModalAddTemplate'}" id="serverAdd"></div>
            <div data-bind="template: { name: 'serverModalRemoveTemplate'}" id="serverRemove"></div>
        </div>

    </div>

    <!-- progressModal modal -->
    <div id="progressModal" class="uk-modal">
        <div class="uk-modal-dialog">
            <a class="uk-modal-close uk-close"></a>
            <div class="uk-progress">
                <div class="uk-progress-bar" style="width: 10%;">10%</div>
            </div>
        </div>
    </div>

    <!-- Message modal -->
    <div id="messageModal" class="uk-modal">
        <div class="uk-modal-dialog">
            <a class="uk-modal-close uk-close"></a>
            <h3 class="message"></h3>
        </div>
    </div>

</div>


</body>
</html>