/*
 * TableInfo.java
 *
 * Created on September 9, 2003, 11:26 PM
 */

package biz.umber.sqldump;

import java.util.*;

/**
 *
 * @author  user
 */
public class TableInfo {

    /** Holds value of property tableName. */
    private String tableName;

    /** Holds value of property dependsOn. */
    private List dependsOn;

    /** Holds value of property parentTables. */
    private List parentTables;

    /** Holds value of property childTables. */
    private List childTables;

    /** Creates a new instance of TableInfo */
    public TableInfo() {
        dependsOn = new ArrayList();
        childTables = new ArrayList();
        parentTables = new ArrayList();
    }

    /** Getter for property tableName.
     * @return Value of property tableName.
     *
     */
    public String getTableName() {
        return this.tableName;
    }

    /** Setter for property tableName.
     * @param tableName New value of property tableName.
     *
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /** Getter for property dependsOn.
     * @return Value of property dependsOn.
     *
     */
    public List getDependsOn() {
        return this.dependsOn;
    }

    /** Setter for property dependsOn.
     * @param dependsOn New value of property dependsOn.
     *
     */
    public void setDependsOn(List dependsOn) {
        this.dependsOn = dependsOn;
    }

    public void addDependsOn(String parentTable) {
        if ( !dependsOn.contains(parentTable) ){
            dependsOn.add(parentTable);
        }
    }

    public String toString() {
        String retValue;

        retValue = "("+tableName + "->" + dependsOn+")\n";
        return retValue;
    }

    /** Getter for property parentTables.
     * @return Value of property parentTables.
     *
     */
    public List getParentTables() {
        return this.parentTables;
    }

    public void updateParentTables(Map masterTableList) {
        Iterator iter = dependsOn.iterator();

        while (iter.hasNext()) {
            String tableName = (String) iter.next();
            TableInfo parentInfo = (TableInfo) masterTableList.get(tableName);
            parentTables.add(parentInfo);
            parentInfo.addChildTable(this);
        }
    }

    void addChildTable(TableInfo childTable) {
        childTables.add(childTable);
    }

    /** Getter for property childTables.
     * @return Value of property childTables.
     *
     */
    public List getChildTables() {
        return this.childTables;
    }

}