/*
 * Created on Jul 1, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package biz.umber.sqldump;

import org.apache.commons.cli.*;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

/**
 * @author navneet
 */
public class DumpOptions {
    private static final String OPT_GZIP = "z";
    private static final String OPT_AUTO_TIMESTAMP = "a";
    private static final String OPT_HELP = "?";
    private static final String OPT_COMPLETE_INSERT = "c";
    private static final String OPT_EXTENDED_INSERT = "e";
    private static final String OPT_HOST_NAME = "h";
    private static final String OPT_LOCK_TABLES = "l";
    private static final String OPT_NO_CREATE_DB = "n";
    private static final String OPT_NO_CREATE_TABLE = "t";
    private static final String OPT_NO_DATA = OPT_NO_CREATE_DB;
    private static final String OPT_PASSWORD = "p";
    private static final String OPT_OUTPUT_FILE = "r";
    private static final String OPT_USER = "u";

    private String database;
    private String host;
    private String user;
    private String pass;
    private boolean needsPassword;
    private boolean help;

    /**
     * @return Returns the database.
     */
    public String getDatabase() {
        return database;
    }
    /**
     * @param database The database to set.
     */
    public void setDatabase(String database) {
        this.database = database;

        if ( database != null && database.trim().length()==0) {
            this.database = null;
        }
    }
    /**
     * @return Returns the host.
     */
    public String getHost() {
        return host;
    }
    /**
     * @param host The host to set.
     */
    public void setHost(String host) {
        this.host = host;

        if ( host != null && host.trim().length()==0) {
            this.host = null;
        }

    }
    /**
     * @return Returns the needsPassword.
     */
    public boolean isNeedsPassword() {
        return needsPassword;
    }
    /**
     * @param needsPassword The needsPassword to set.
     */
    public void setNeedsPassword(boolean needsPassword) {
        this.needsPassword = needsPassword;
    }
    /**
     * @return Returns the pass.
     */
    public String getPass() {
        return pass;
    }
    /**
     * @param pass The pass to set.
     */
    public void setPass(String pass) {
        this.pass = pass;
    }
    /**
     * @return Returns the user.
     */
    public String getUser() {
        return user;
    }
    /**
     * @param user The user to set.
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * Holds value of property outputFile.
     */
    private String outputFile = "data";

    private static final String BOTH = "Both";
    /**
     * constant used as flag
     */
    private static final String DATA = "Data";
    private static final String STRUCTURE = "Structure";
    private boolean autoTimeStamp;
    /**
     * Hold wheteher to dump table data only
     * or structure data only. default to BOTH
     */
    private String dataOrStructure = BOTH;
    /**
     * Holds value of property extendedInsert.
     */
    private boolean extendedInsert = false;
    /**
     * Holds value of property generateZip.
     */
    private boolean generateZip = false;

    private Options cmdOpt = new Options();
    private boolean lockTables;


    /**
     *
     */
    public DumpOptions() {
        initOptions();
    }

    /*    public void parseOptions(String[] args) {

            int argCounter;
            for (argCounter = 0; argCounter < args.length; argCounter++) {
                String opt = args[argCounter];

                // not an option. Stop processing
                if (!opt.startsWith("-")) {
                    break;
                }

                if (opt.startsWith("-h")) {
                    // next param has host name
                    setHost(args[++argCounter]);
                }else if (opt.equals("-a")) {
                    //next param has TimeStamp
                    setAutoTimeStamp(true);
                }else if (opt.startsWith("-u")) {
                    // next param has user name
                    setUser(args[++argCounter]);
                } else if (opt.startsWith("-o")) {
                    // next param has output filename
                    setOutputFile(args[++argCounter]);
                } else if (opt.equals("-p")) {
                    setNeedsPassword(true);
                } else if (opt.equals("-e")) {
                    setExtendedInsert(true);
                } else if (opt.startsWith("-t")) {
                    if (opt.equals("-tb")) {
                        setDataOrStructure(BOTH);
                    } else if(opt.equals("-ts")) {
                        setDataOrStructure(STRUCTURE);
                    }else if(opt.equals("-td")) {
                        setDataOrStructure(DATA);
                    }
                }else if(opt.equals("-z")) {
                    setGenerateZip(true);
                }
            }
            if ( argCounter < args.length && getDatabase()==null) {
                setDatabase(args[++argCounter]);
            }
        }
    */
    public void parseOptions(String[] args) {
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;

        try {
            line = parser.parse(cmdOpt,args);
            if (line.hasOption(OPT_HELP)) {
                setHelp(true);
            }
            if (line.hasOption(OPT_HOST_NAME)) {
                // next param has host name
                setHost(line.getOptionValue(OPT_HOST_NAME));
            }
            if (line.hasOption(OPT_USER)) {
                // next param has user name
                setUser(line.getOptionValue(OPT_USER));
            }

            if (line.hasOption(OPT_OUTPUT_FILE)) {
                // next param has output filename
                setOutputFile(line.getOptionValue(OPT_OUTPUT_FILE));
                // only if there is an output file specified can we timestamp it
                if (line.hasOption(OPT_AUTO_TIMESTAMP)) {
                    //next param has TimeStamp
                    setAutoTimeStamp(true);
                }
                // only if there is an output file specified can we gzip it
                if(line.hasOption(OPT_GZIP)) {
                    setGenerateZip(true);
                }
            }

            if (line.hasOption(OPT_PASSWORD)) {
                setNeedsPassword(true);
            }

            if (line.hasOption(OPT_EXTENDED_INSERT)) {
                setExtendedInsert(true);
            }

            if (line.hasOption(OPT_NO_CREATE_TABLE)) {
                setDataOrStructure(DATA);
            }

            if (line.hasOption(OPT_NO_DATA)) {
                setDataOrStructure(STRUCTURE);
            }

            if (line.hasOption(OPT_LOCK_TABLES)) {
                setLockTables(true);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if ( line != null) {
            String[] otherParams = line.getArgs();
            if (otherParams.length==1) {
                setDatabase(otherParams[0]);
            }
        }
    }
    /**
     *
     */
    private void initOptions() {
        cmdOpt.addOption(OPT_HELP,"help",false,"show this text");

        //cmdOpt.addOption(createOptionWithOnlyLongName("add-drop-table",false,"Add a DROP TABLE statement before each CREATE TABLE statement."));
        //cmdOpt.addOption(createOptionWithOnlyLongName("add-locks",false,"Surround each table dump with LOCK TABLES and UNLOCK TABLES statements. This results in faster inserts when the dump file is reloaded."));
        //cmdOpt.addOption(OPT_COMPLETE_INSERT,"complete-insert",false,"Use complete INSERT statements that include column names.");
        cmdOpt.addOption(OPT_EXTENDED_INSERT,"extended-insert",false,"Use multiple-row INSERT syntax that include several VALUES lists. This results in a smaller dump file and speeds up inserts when the file is reloaded.");
        cmdOpt.addOption(createOptionWithRequiredParam(OPT_HOST_NAME,"host","hostname","Dump data from the MySQL server on the given host. The default host is localhost."));
        cmdOpt.addOption(OPT_LOCK_TABLES,"lock-tables",false,"Lock all tables before starting the dump. The tables are locked with READ LOCAL to allow concurrent inserts in the case of MyISAM tables. Please note that when dumping multiple databases, --lock-tables locks tables for each database separately. So, using this option will not guarantee that the tables in the dump file will be logically consistent between databases. Tables in different databases may be dumped in completely different states.");
        //cmdOpt.addOption(OPT_NO_CREATE_DB,"no-create-db",false,"This option suppresses the CREATE DATABASE /*!32312 IF NOT EXISTS*/ db_name statements that are otherwise included in the output if the --databases or --all-databases option is given.");
        OptionGroup dataOrStructureGroup = new OptionGroup();
        dataOrStructureGroup.addOption(new Option(OPT_NO_CREATE_TABLE,"no-create-info",false,"Don't write CREATE TABLE statements that re-create each dumped table."));
        dataOrStructureGroup.addOption(new Option(OPT_NO_DATA,"no-data",false,"Don't write any row information for the table. This is very useful if you just want to get a dump of the structure for a table."));
        cmdOpt.addOptionGroup(dataOrStructureGroup);
        cmdOpt.addOption(OPT_PASSWORD, "password", false,
                "Need to use a password when connecting to the server. " +
                        "You will be prompted for the password ont he commandline.");
        Option outputFileOption =new Option(OPT_OUTPUT_FILE,"result-file",true,"Direct output to a given file. This option should be used on Windows, because it prevents newline `\\n' characters from being converted to `\\r\\n' carriage return/newline sequences.");
        outputFileOption.setArgName("filename");
        cmdOpt.addOption(outputFileOption);
        cmdOpt.addOption(createOptionWithRequiredParam(OPT_USER,"user","username","The MySQL username to use when connecting to the server."));
        cmdOpt.addOption(OPT_AUTO_TIMESTAMP,"auto-timestamp",false,"add the timestamp to the name of the file that is specified in the " + OPT_OUTPUT_FILE + " option");
        cmdOpt.addOption(OPT_GZIP,"generate-gzip",false,"compress the output into a gz file");
    }

    /**
     * @param optName
     * @param longOpt
     * @param desc
     * @return
     */
    private Option createOptionWithOptionalParam(String optName, String longOpt, String paramName, String desc) {
        return OptionBuilder.withLongOpt(longOpt)
                .withValueSeparator()
                .hasOptionalArg()
                .withArgName(paramName)
                .withDescription(desc)
                .create(optName);
    }

    /**
     * @param optName
     * @param longOpt
     * @param desc
     * @return
     */
    private Option createOptionWithRequiredParam(String optName, String longOpt, String paramName, String desc) {
        return OptionBuilder.withLongOpt(longOpt)
                .withValueSeparator()
                .hasArg()
                .withArgName(paramName)
                .withDescription(desc)
                .create(optName);
    }

    /**
     * @param optName
     * @param longOpt
     * @param desc
     * @return
     */
    private Option createOptionWithOnlyLongName(String longOpt, boolean hasArg, String desc) {
        return OptionBuilder.withLongOpt(longOpt)
                .hasArg(hasArg).withDescription(desc).create();
    }

    public void printOptions(String appSyntax) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(appSyntax, cmdOpt);
    }

    /**
     * @return Returns the autoTimeStamp.
     */
    public boolean isAutoTimeStamp() {
        return autoTimeStamp;
    }
    /**
     * @param autoTimeStamp The autoTimeStamp to set.
     */
    public void setAutoTimeStamp(boolean autoTimeStamp) {
        this.autoTimeStamp = autoTimeStamp;
    }
    /**
     * @return Returns the dataOrStructure.
     */
    public String getDataOrStructure() {
        return dataOrStructure;
    }
    /**
     * @param dataOrStructure The dataOrStructure to set.
     */
    public void setDataOrStructure(String dataOrStructure) {
        this.dataOrStructure = dataOrStructure;
    }

    public boolean isData() {
        return this.dataOrStructure.equals(DATA) || this.dataOrStructure.equals(BOTH);
    }

    public boolean isStructure() {
        return this.dataOrStructure.equals(DATA) || this.dataOrStructure.equals(BOTH);
    }

    /**
     * @return Returns the extendedInsert.
     */
    public boolean isExtendedInsert() {
        return extendedInsert;
    }
    /**
     * @param extendedInsert The extendedInsert to set.
     */
    public void setExtendedInsert(boolean extendedInsert) {
        this.extendedInsert = extendedInsert;
    }
    /**
     * @return Returns the generateZip.
     */
    public boolean isGenerateZip() {
        return generateZip;
    }
    /**
     * @param generateZip The generateZip to set.
     */
    public void setGenerateZip(boolean generateZip) {
        this.generateZip = generateZip;
    }
    /**
     * @return Returns the outputFile.
     */
    public String getOutputFile() {
        return outputFile;
    }
    /**
     * @param outputFile The outputFile to set.
     */
    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }
    /**
     * @return Returns the help.
     */
    public boolean isHelp() {
        return help;
    }
    /**
     * @param help The help to set.
     */
    public void setHelp(boolean help) {
        this.help = help;
    }
    /**
     * @return Returns the lockTables.
     */
    public boolean isLockTables() {
        return lockTables;
    }

    /**
     * @param lockTables The lockTables to set.
     */
    public void setLockTables(boolean lockTables) {
        this.lockTables = lockTables;
    }
}