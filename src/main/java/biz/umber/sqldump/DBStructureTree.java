/*
 * DBStructureTree.java
 *
 * Created on September 9, 2003, 7:08 PM
 */

package biz.umber.sqldump;

import java.sql.*;
import java.util.*;
import java.util.logging.*;

/**
 *
 * @author  user
 */
public class DBStructureTree {

    public static final Logger log = Logger.getLogger(DBStructureTree.class.getName());
    static Map tableMap = null;
    static String[] orderedTableNames = null;

    /** Creates a new instance of DBStructureTree */
    private DBStructureTree() {
    }

    synchronized public static final String[] getOrderedTableNames(Connection connection) throws SQLException {
        if ( connection == null ) {
            return null;
        }

        if ( orderedTableNames != null ) {
            return orderedTableNames;
        }

        Map tableList = getTableList(connection);
        log.fine("Table list:"+tableList.values());
        fillForeignKeyInfo(connection, tableList);
        log.fine("Table list with fk:"+tableList.values());
        processAllTables(tableList);
        log.fine("Trying to find circular dependencies.");
        List orderedList = getCorrectOrder(tableList);
        if ( orderedList.size() == 0 ) {
            return new String[0];
        }

        log.fine("Ordered Table list with fk:"+orderedList);
        orderedTableNames = new String[orderedList.size()];
        for(int i=0; i < orderedList.size(); i++) {
            TableInfo info = (TableInfo) orderedList.get(i);
            orderedTableNames[i] = info.getTableName();
        }

        return orderedTableNames;
    }

    private static final Map getTableList(Connection connection) throws SQLException {
        DatabaseMetaData dmd = connection.getMetaData();
        if ( tableMap != null) {
            return tableMap;
        }

        tableMap = new TreeMap();
        log.fine("Trying to get table information from the database");
        ResultSet rs = dmd.getTables(null, null, null, new String[]{"TABLE"});

        while (rs.next()) {
            TableInfo info = new TableInfo();
            info.setTableName(rs.getString("TABLE_NAME"));
            tableMap.put(info.getTableName(),info);
        }
        log.fine("Got table information from the database:"+tableMap);

        return tableMap;
    }

    private static final void fillForeignKeyInfo(Connection connection, Map tableInfoList) throws SQLException {
        Iterator iter = tableInfoList.values().iterator();
        DatabaseMetaData dmd = connection.getMetaData();
        while( iter.hasNext() ) {
            TableInfo info = (TableInfo) iter.next();
            ResultSet rs = dmd.getImportedKeys(null, null, info.getTableName());

            while (rs.next()) {
                info.addDependsOn(rs.getString("PKTABLE_NAME"));
            }
        }
    }

    private static final void processAllTables(Map tableList) {
        Iterator iter = tableList.values().iterator();
        while( iter.hasNext() ) {
            TableInfo info = (TableInfo) iter.next();
            info.updateParentTables(tableList);
        }
    }

    private static final List getCorrectOrder(Map tableList) throws SQLException {
        List nodesProcessed = new ArrayList();
        List nodesToBeProcessed = new ArrayList(tableList.values());

        int lastCount = 0;
        int i = 0;
        while ( !nodesToBeProcessed.isEmpty() ) {
            // always process only the nodes in the list
            // and continue till all nodes processed
            i %= nodesToBeProcessed.size();
            if ( i==0 ) {
                if ( lastCount == nodesToBeProcessed.size() ) {
                    log.severe("There is atleast one circular dependency in the tables. Check the foreign keys in the tables.");
                    throw new SQLException("Circular depepndency identified in the tables."+nodesToBeProcessed);
                } else {
                    lastCount = nodesToBeProcessed.size();
                }

                log.fine(nodesToBeProcessed.size()+" tables remain to be processed.");
            }
            TableInfo info = (TableInfo) nodesToBeProcessed.get(i);
            log.fine("Looking at table:"+info);
            List parentNodes = info.getParentTables();
            if ( haveProcessedAll(info, parentNodes,nodesProcessed) ) {
                log.fine("have processed all parents, so can process this now.");
                nodesProcessed.add(info);
                nodesToBeProcessed.remove(i);
            } else {
                log.fine("can not process this now since parent nodes still left.");
                // look at the next node
                i ++;
            }
        }



        return nodesProcessed;
    }

    private static final boolean haveProcessedAll(TableInfo currentNode, List nodeList, List checkList) {
        boolean processedAll = true;

        Iterator iter = nodeList.iterator();
        while ( processedAll && iter.hasNext()) {
            TableInfo node = (TableInfo) iter.next();
            processedAll = checkList.contains(node) || node.getTableName().equals(currentNode.getTableName());
        }

        return processedAll;
    }

}