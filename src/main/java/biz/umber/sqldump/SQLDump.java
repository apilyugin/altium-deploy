/*
 * SQLDump.java
 *
 * Created on August 28, 2003, 5:29 PM
 */
package biz.umber.sqldump;

import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.regex.*;
import java.util.zip.*;
import java.util.logging.*;

/**
 *
 * @author  navneet
 */
public class SQLDump {

    public static final String DRIVER = "org.gjt.mm.mysql.Driver";
    private DumpOptions options = new DumpOptions();

    public static final SimpleDateFormat fullYearDateTime =
            new SimpleDateFormat("yyyyMMdd-HHmmss");
    private static final Pattern crPattern = Pattern.compile("\r");
    private static final Pattern lfPattern = Pattern.compile("\n");
    private static final Pattern quotePattern = Pattern.compile("'");
    public static final Logger log = Logger.getLogger(SQLDump.class.getName());

    Connection connection = null;
    PrintStream out = System.out;

    public void connect() throws SQLException {
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(getURL(), options.getUser(), options.getPass());
        } catch (ClassNotFoundException cnfe) {
            throw new SQLException("Driver not found: " + DRIVER);
        }
    }

    public void disconnect() {
        if (connection != null) {
            try{
                connection.close();
            } catch(SQLException sqle) {
                log.severe("Exception while closing connection. Nothing serious. Ignore." + sqle.getMessage());
            }
        }
    }

    public void lockAllTables() throws SQLException {
        if ( connection == null ) {
            return;
        }

        // try locking only if asked for
        if ( options.isLockTables() ) {
            String [] tableNames = DBStructureTree.getOrderedTableNames(connection);
            if ( tableNames.length == 0 ) {
                return;
            }
            String lockQuery = "LOCK TABLE ";
            for(int i = 0; i < tableNames.length; i++) {

                if( i > 0 ) { lockQuery += ","; }

                lockQuery += tableNames[i] + " WRITE";
            }

            log.info("Trying to lock "+ tableNames.length + " tables before backup.");
            log.info(lockQuery);
            Statement stmt = connection.createStatement();
            int lockedTables = stmt.executeUpdate(lockQuery);
            log.info("Locked tables. No of locked tables: "+lockedTables);
        }
    }

    public void unlockAllTables() throws SQLException {
        if ( connection == null ) {
            return;
        }

        if ( !options.isLockTables() ) {
            return;
        }

        String [] tableNames = DBStructureTree.getOrderedTableNames(connection);

        if ( tableNames.length == 0 ) {
            return;
        }

        String unlockQuery = "UNLOCK TABLES";
        log.info("Trying to unlock "+ tableNames.length + " tables after backup.");
        Statement stmt = connection.createStatement();
        int unlockedTables = stmt.executeUpdate(unlockQuery);
        log.info("Unlocked tables. No of unlocked tables: "+unlockedTables);
    }

    public void dumpAllTables() throws SQLException {
        if ( connection == null ) {
            return;
        }
        String [] tableNames=null;
        tableNames = DBStructureTree.getOrderedTableNames(connection);

        if ( tableNames.length == 0 ) {
            return;
        }

        dumpDataStructure(tableNames,options.getDataOrStructure());
    }

    public void dumpDataStructure(String[] tableNames,String dataOrStructure)
            throws SQLException {
        for ( int i=0;i<tableNames.length;i++){
            log.info("Start dumping table: " + tableNames[i]);
            dumpTable(tableNames[i], out);
            log.info("Done dumping table: " + tableNames[i]);
        }
    }

    public void dumpTable(String tableName, PrintStream out)
            throws SQLException {
        if ( connection == null ) {
            return;
        }
        if(options.isStructure()){
            String tableCreateSyntax = getTableCreateInfo(connection,tableName);
            writeOutput(out, "# drop table "+tableName);
            writeOutput(out, "DROP TABLE IF EXISTS " + tableName);
            writeOutput(out, "# create table "+tableName);
            writeOutput(out, tableCreateSyntax);
            writeOutput(out, "");
            writeOutput(out, "");
        }
        if(options.isData()){
            String query="";
            query = "select * from " + tableName;
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            // only if we have data, start output
            if ( rs.next() ){
                writeOutput(out, "# start dumping data for " + tableName);
                if ( options.isExtendedInsert() ) {
                    writeOutput(out, "/*!40000 ALTER TABLE "+tableName+" DISABLE KEYS */");
                    writeOutput(out, "LOCK TABLES "+tableName+" WRITE");
                }

                outputExtendedInsertQuery(rs, tableName, out);
                if ( options.isExtendedInsert() ) {
                    writeOutput(out, "UNLOCK TABLES");
                    writeOutput(out, "/*!40000 ALTER TABLE "+tableName+" ENABLE KEYS */");
                }
                writeOutput(out, "# done dumping data for " + tableName);
                writeOutput(out, "");
                writeOutput(out, "");
            }//end of rs.next()
        }//end if.
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        // check parameters
        // if filename is specified, use that file
        SQLDump d = new SQLDump();

        if (!d.processArgs(args)) {
            d.writeParameterList();
            return;
        }

        try {
            d.connect();
            d.lockAllTables();
            d.dumpAllTables();
            d.unlockAllTables();
        }catch(SQLException sqle){
            System.err.println("Error while dumping data");
            System.err.println("Please pass the message to a support person.");

            log.severe(sqle.getMessage());
            sqle.printStackTrace();
        }finally {
            d.disconnect();
            if ( d.out != System.out ) {
                d.out.close();
            }
        }

        System.out.println("Done all.");
    }

    private boolean processArgs(String[] args)
            throws Exception {
        boolean retval = true;

        options.parseOptions(args);

        try {
            validateOptions();

            processOptions();
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
            retval = false;
        }

        return retval;
    }

    /**
     * @param d
     * @throws Exception
     */
    private void validateOptions() throws Exception {

        if ( options.getDatabase() == null ) {
            throw new Exception("Database name not specified");
        }
        String outputFile = options.getOutputFile();
        if ( outputFile == null || outputFile.trim().length()==0) {
            throw new Exception("Output file not specified");
        }

    }

    /**
     */
    private void processOptions() throws IOException {
        if (options.isNeedsPassword()) {
            System.out.print("Enter password:");
            LineNumberReader lis = new LineNumberReader(new InputStreamReader(System.in));
            options.setPass(lis.readLine());
        }

        String outputFile = options.getOutputFile();
        if (options.getHost() == null) {
            options.setHost("127.0.0.1");
        }

        if(options.isAutoTimeStamp()){

            String toDay = fullYearDateTime.format(new java.util.Date());
            outputFile = outputFile + "-" + toDay;
        }

        if ( options.isGenerateZip() ) {
            outputFile += ".sql.gz";
        } else {
            outputFile += ".sql";
        }

        // open the file for output
        if ( outputFile != null ) {
            log.info("Sending output to file: " + outputFile);
            OutputStream os = new FileOutputStream(outputFile);
            if ( options.isGenerateZip() ) {
                os = new GZIPOutputStream(os);
            }

            out = new PrintStream(os);
        }
    }

    public void outputExtendedInsertQuery(ResultSet rs, String tableName,
                                          PrintStream out) throws SQLException {

        String[] fieldNames = null;
        String fields = "";

        if ( fieldNames == null ) {
            fieldNames = getColumnNames(rs);
            fields = joinFieldNames(fieldNames);
        }

        StringBuffer query = new StringBuffer();

        // can't go over 1000 records in extended inserts
        // or 64k length of string
        int recCount = 0;
        if (rs.isBeforeFirst()) {
            rs.next();
        }

        if ( ! rs.isAfterLast() ) {
            do {
                if (recCount == 0 ) {
                    query.append("insert into ").append(tableName).append(" (")
                            .append(fields).append(") values ");
                } else {
                    query.append(" , ");
                }
                recCount ++;
                makeInsertQueryString(rs, tableName, fieldNames, query);

                // if this is not in Extended insert mode
                // or we hit 1000 records
                // then we need to finish this statement
                if ( !options.isExtendedInsert() || recCount >= 1000 ) {
                    writeOutput(out, "# writing query for "+recCount+" records ("+query.length()+" chars)");
                    writeOutput(out, query.toString());
                    query.setLength(0);
                    recCount = 0;
                }
            } while (rs.next());

            if ( options.isExtendedInsert() && query.length() > 0 ) {
                writeOutput(out, "# writing query for "+recCount+" records ("+query.length()+" chars)");
                writeOutput(out, query.toString());
            }
        }
    }

    public void makeInsertQueryString(ResultSet rs, String tableName,
                                      String[] fieldNameList, StringBuffer query) throws SQLException {
        String[] fieldNames = fieldNameList;

        if ( (fieldNames == null) || (fieldNames.length==0) ) {
            fieldNames = getColumnNames(rs);
        }

        // make a comma separated string for field names
        StringBuffer values = new StringBuffer();
        boolean needComma = false;
        int colCount = fieldNames.length;
        for ( int i=0; i < colCount; i++ ) {
            String data = rs.getString(fieldNames[i]);

            if ( needComma ) {
                values.append(",");
            }

            if ( data != null ) {
                // fix the single quote handling
                data = convertString(data);
                //replace ; with , from the data.
                data=replaceSemicolonWithComma(data);
                values.append("'").append(data).append("'");
                needComma = true;
            } else {
                values.append("NULL");
                needComma = true;
            }
        }
        query.append("(").append(values).append(")");
    }

    public String replaceSemicolonWithComma(String data){
        char oldChar=';';
        char newChar=',';
        return data.replace(oldChar, newChar);
    }

    public String[] getColumnNames(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int colCount = rsmd.getColumnCount();
        String[] retval = new String[colCount];

        for (int i =0; i < colCount; i++ ) {
            retval[i] = rsmd.getColumnName(i+1);
        }

        return retval;
    }

    public String getTableCreateInfo(Connection connection, String tableName)
            throws SQLException {
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SHOW CREATE TABLE " + tableName);

        String query = null;
        if ( rs.next() ) {
            // second column contains the string we need
            query = rs.getString(2);
        }
        return query;
    }

    public String getURL() {
        return "jdbc:mysql://"+options.getHost()+"/"+options.getDatabase();
    }

    public void writeOutput(PrintStream out, String queryParam) {
        String query = queryParam;

        if ( query == null ) {
            query="";
        }

        // add a semi colon for non-comment and non-blank lines
        if ( (query.length()>0) && !query.startsWith("#") ) {
            query += ';';
        }

        if ( out != null ) {
            out.println(query);
        }

        if ( log != null ) {
            log.fine(query);
        }
    }

    String joinString(String[] strArr, String joinString) {
        StringBuffer buffer = new StringBuffer();

        for( int i =0; i < strArr.length; i++) {
            if ( i > 0 ) { buffer.append(joinString); }
            buffer.append(strArr[i]);
        }

        return buffer.toString();
    }

    String joinFieldNames(String[] strArr) {
        StringBuffer buffer = new StringBuffer();

        for( int i =0; i < strArr.length; i++) {
            if ( i > 0 ) { buffer.append(","); }
            buffer.append("`" + strArr[i] + "`");
        }

        return buffer.toString();
    }

    String convertString(String dataParam) {
        String data = dataParam;
        data = quotePattern.matcher(data).replaceAll("''");
        // remove all carriage returns
        Matcher m = crPattern.matcher(data);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "");
            sb.append("\\r");
        }
        m.appendTail(sb);
        data = sb.toString();

        // remove all new lines
        m = lfPattern.matcher(data);
        sb.setLength(0);
        while (m.find()) {
            m.appendReplacement(sb, "");
            sb.append("\\n");
        }
        m.appendTail(sb);
        data = sb.toString();
        return data;
    }

    public void writeParameterList() {
/*        System.out.println("Usage: java -jar sqldump.jar [options] <databaseName>");
        System.out.println("where options are as follows:");
        System.out.println("-h<hostname> the host that contains the database");
        System.out.println("-a add the timestamp to the name of the file that is specified in the -o clause");
        System.out.println("-u<user> the user name used to connect to the database ");
        System.out.println("-o<filename> the file where the output is written (default will write to console)");
        System.out.println("-p will make the application wait for the password");
        System.out.println("-e use extended inserts (mysql syntax)");
        System.out.println("-z compress the output into a gz file");
        System.out.println("and any one of the following:");
        System.out.println("-td output only data");
        System.out.println("-ts output only structure of the database");
        System.out.println("-tb output both (default)");
*/
        options.printOptions("java -jar sqldump.jar [options] <databaseName>");
    }

}
