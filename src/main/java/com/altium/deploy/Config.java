package com.altium.deploy;


import com.altium.deploy.error.ConfigException;
import org.apache.commons.cli.CommandLine;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class Config {


    private static Properties properties = new Properties();
    private static Config instance;

    public static String THEMES_DIRECTORY = "themes";
    public static String THEMES_PATH = "sites/all/" + THEMES_DIRECTORY;
    public static String MODULES_DIRECTORY = "modules";
    public static String MODULES_PATH = "sites/all/" + MODULES_DIRECTORY;

    private Config(){

    }

    public static Config getInstance(){
        if(instance == null){
            instance = new Config();
        }
        return instance;
    }

    public static void loadConfig(String propertyFile) throws IOException{
        properties.load(new FileInputStream(propertyFile));
    }
    public static void loadConfig(CommandLine commandLine) throws IOException, ConfigException{
        if(!commandLine.hasOption("c")){
            throw new ConfigException("Invalid start params");
        }
        properties.load(new FileInputStream(commandLine.getOptionValue("c")));
    }



    public String getDbUrl(){
        return "jdbc:mysql://"+ properties.getProperty("dbHost") + "/" + properties.getProperty("dbName") + "?useUnicode=true&characterEncoding=UTF-8&characterSetResults=utf8&connectionCollation=utf8_general_ci";
    }

    public String getDbUserName(){
        return properties.getProperty("dbUser");
    }

    public String getDbPassword(){
        return properties.getProperty("dbPassword");
    }

    public String getDbName(){
        return properties.getProperty("dbName");
    }

    public String getBackupDir(){
        return properties.getProperty("dumpDirectory");
    }

    public String getZipDir(){
        return properties.getProperty("archiveDirectory");
    }

    public String getDeployDir(){
        return properties.getProperty("deployDirectory");
    }

    public String getScpHostName(){
        return properties.getProperty("scpHost");
    }

    public String getScpPort(){
        return properties.getProperty("scpPort");
    }

    public String getScpUser(){
        return properties.getProperty("scpUser");
    }

    public String getScpPassword(){
        return properties.getProperty("scpPassword");
    }

    public String getScpTargetDir(){
        return properties.getProperty("scpTargetDir");
    }

    public String getLogDir(){
        return properties.getProperty("logDirectory");
    }

    public String getAltiumDir(){
        return properties.getProperty("altiumDirectory");
    }

    public String getMysqlPath(){
        return properties.getProperty("mysqlPath");
    }

}
