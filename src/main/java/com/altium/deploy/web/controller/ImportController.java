package com.altium.deploy.web.controller;

import com.altium.deploy.db.ImportService;
import com.altium.deploy.db.UploadService;
import com.altium.deploy.db.model.ImportModel;
import com.altium.deploy.db.model.UploadModel;
import com.altium.deploy.error.AltiumDBException;
import com.altium.deploy.process.Executor;
import com.altium.deploy.process.jdbc.JDBCImport;
import com.altium.deploy.process.runtime.mysql.Import;
import com.altium.deploy.web.response.Response;
import com.altium.deploy.web.response.impl.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.util.HashMap;

@Controller
public class ImportController {

    @Autowired
    private UploadService uploadService;
    @Autowired
    private ImportService importService;
    @Autowired
    private TaskExecutor taskExecutor;
    @Autowired
    private JDBCImport jdbcImport;
    @Autowired
    private Import anImport;


    @RequestMapping(value = "/import", method = RequestMethod.GET)
    public
    @ResponseBody
    Response dumpListRequest() {
        Response response = new JsonResponse();
        try {
            final ResultSet list = importService.getList();
            while (list.next()) {
                response.addData(new HashMap<String, String>() {{
                    put("id", list.getString(1));
                    put("uploadId", list.getString(2));
                    put("status", list.getString(3));
                    put("startedOn", list.getTimestamp(4).toString());
                    put("finishedOn", list.getTimestamp(5).toString());
                }});
            }
        } catch (Exception e) {
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/import/upload/{uploadId}", method = RequestMethod.POST)
    public
    @ResponseBody
    Response importRequest(@PathVariable("uploadId") String uploadId) {
        Response importResponse = new JsonResponse();
        try {

            UploadModel uploadModel = uploadService.getById(uploadId);
            if (uploadModel.getId() == null) {
                throw new AltiumDBException("Invalid id");
            }
            ImportModel importModel = importService.add(uploadId);
            taskExecutor.execute(new Executor(anImport,importModel));
            importResponse.addData(importModel.getData());
        } catch (Exception e) {
            importResponse.setErrorMessage(e.getMessage());
        }
        return importResponse;
    }

    @RequestMapping(value = "/import/{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    Response getByIdRequest(@PathVariable String id) {
        Response response = new JsonResponse();
        try {
            ImportModel importModel = importService.getById(id);
            response.addData(importModel.getData());
        } catch (Exception e) {
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

}
