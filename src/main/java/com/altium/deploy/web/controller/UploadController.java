package com.altium.deploy.web.controller;


import com.altium.deploy.db.UploadService;
import com.altium.deploy.db.model.UploadModel;
import com.altium.deploy.Config;
import com.altium.deploy.web.model.UploadRequestModel;
import com.altium.deploy.web.response.Response;
import com.altium.deploy.web.response.UploadResponse;
import com.altium.deploy.web.response.impl.JsonResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.util.HashMap;

@Controller
public class UploadController {

    @Autowired
    private UploadService uploadService;

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    public @ResponseBody Response dumpListRequest(){
        JsonResponse response = new JsonResponse();
        try {
            final ResultSet list = uploadService.getList();
            while (list.next()){
                response.addData(new HashMap<String, String>(){{
                    put("id",list.getString(1));
                    put("createdOn",list.getTimestamp(3).toString());
                }});
            }
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/upload/create", method = RequestMethod.POST)
    public @ResponseBody Response uploadRequest(@ModelAttribute("file")UploadRequestModel uploadModel, BindingResult bindingResult){
        UploadResponse response = new UploadResponse();
        try {
            Config config = Config.getInstance();
            MultipartFile uploadedFile = uploadModel.getFile();
            File targetFile = new File(config.getDeployDir() + "/" + uploadedFile.getOriginalFilename());
            if(!targetFile.exists()){
                targetFile.createNewFile();
            }
            IOUtils.copy(uploadedFile.getInputStream(),new FileOutputStream(targetFile));
            UploadModel uploadedDbModel = uploadService.addUpload(uploadedFile.getOriginalFilename());
            response.addData(uploadedDbModel.getData());
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

}
