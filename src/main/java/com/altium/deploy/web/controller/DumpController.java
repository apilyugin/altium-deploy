package com.altium.deploy.web.controller;


import com.altium.deploy.db.DumpService;
import com.altium.deploy.db.ServerService;
import com.altium.deploy.db.model.DumpModel;
import com.altium.deploy.db.model.ServerModel;
import com.altium.deploy.Config;
import com.altium.deploy.action.Backup;
import com.altium.deploy.helper.Misc;
import com.altium.deploy.process.Executor;
import com.altium.deploy.process.jdbc.JDBCDump;
import com.altium.deploy.process.runtime.mysql.Dump;
import com.altium.deploy.web.response.DumpListResponse;
import com.altium.deploy.web.response.DumpResponse;
import com.altium.deploy.web.response.Response;
import com.altium.deploy.web.response.impl.JsonResponse;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.sql.ResultSet;
import java.util.HashMap;

@Controller
public class DumpController {

    @Autowired
    private DumpService dumpService;
    @Autowired
    private ServerService serverService;
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    private JDBCDump jdbcDump;
    @Autowired
    private Dump dump;

    @RequestMapping(value = "/dump", method = RequestMethod.GET)
    public @ResponseBody Response dumpListRequest(){
        DumpListResponse response = new DumpListResponse();
        try {
            final ResultSet list = dumpService.getList();
            while (list.next()){
                response.addData(new HashMap<String, String>(){{
                    put("id",list.getString(1));
                    put("filename",list.getString(2));
                    put("status",list.getString(3));
                    put("createdOn",list.getTimestamp(4).toString());
                }});
            }
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/dump/{id}", method = RequestMethod.GET)
    public @ResponseBody Response dumpListRequest(@PathVariable String id){
        DumpResponse response = new DumpResponse();
        try {
            DumpModel dumpModel = dumpService.getById(id);
            response.addData(dumpModel.getData());
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }


    @RequestMapping(value = "/dump", method = RequestMethod.POST)
    public @ResponseBody Response dumpRequest(){
        DumpResponse response = new DumpResponse();
        try{
            Config config = Config.getInstance();
            Backup backup = new Backup(config);
            //Check and create if need dump directory
            String backupDirName = Misc.getFormatterDateString();
            DumpModel dumpModel = dumpService.addDump(backupDirName);
            taskExecutor.execute(new Executor(dump,dumpModel));
            response.addData(dumpModel.getData());
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/dump/{dump_id}/server/{server_id}")
    public @ResponseBody Response uploadDump(@PathVariable String dump_id, @PathVariable String server_id){
        Response response = new JsonResponse();
        try{
            Config config = Config.getInstance();
            DumpModel dump = dumpService.getById(dump_id);
            ServerModel serverModel = (ServerModel)serverService.getById(server_id);
            String filename = dump.getFilename();
            String dumpDir = config.getZipDir();

            HttpClient httpClient = new HttpClient();
            PostMethod post = new PostMethod(serverModel.getUrl() + ":" + serverModel.getPort() + "/upload/create");
            File dumpFile = new File(dumpDir + "/" + filename + DumpModel.DUMP_EXTENSION);
            Part[] parts = {
                new FilePart("file",dumpFile)
            };
            post.setRequestEntity(
                    new MultipartRequestEntity(parts,post.getParams())
            );
            int i = httpClient.executeMethod(post);
            String m = "x";
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }
}
