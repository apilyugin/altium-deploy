package com.altium.deploy.web.controller;


import com.altium.deploy.db.ServerService;
import com.altium.deploy.db.model.DbModel;
import com.altium.deploy.db.model.ServerModel;
import com.altium.deploy.web.response.Response;
import com.altium.deploy.web.response.impl.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.ResultSet;
import java.util.HashMap;

@Controller
public class ServerController {

    @Autowired
    protected ServerService serverService;

    @RequestMapping(value = "/server",method = RequestMethod.GET)
    public @ResponseBody Response indexAction(){
        Response response = new JsonResponse();
        try {
            final ResultSet list = serverService.getList();
            while (list.next()){
                response.addData(new HashMap<String, String>(){{
                    put("id",list.getString(1));
                    put("name",list.getString(2));
                    put("url",list.getString(3));
                    put("port",list.getString(4));
                }});
            }
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/server/{id}",method = RequestMethod.GET)
    public @ResponseBody Response getAction(@PathVariable String id){
        Response response = new JsonResponse();
        try {
            DbModel serverModel = serverService.getById(id);
            response.addData(serverModel.getData());
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/server", method = RequestMethod.POST)
    public @ResponseBody Response postAction(ServerModel serverModel){
        Response response = new JsonResponse();
        try {
            DbModel addModel = serverService.add(serverModel);
            response.addData(addModel.getData());
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/server/{id}", method = RequestMethod.PUT)
    public @ResponseBody Response putAction(@PathVariable String id, ServerModel serverModel){
        Response response = new JsonResponse();
        try {
            DbModel updateModel = serverService.update(serverModel);
            response.addData(updateModel.getData());
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/server/{id}", method = RequestMethod.DELETE)
    public @ResponseBody Response deleteAction(@PathVariable String id){
        Response response = new JsonResponse();
        try {
            serverService.delete(id);
        } catch (Exception e){
            response.setErrorMessage(e.getMessage());
        }
        return response;
    }

}
