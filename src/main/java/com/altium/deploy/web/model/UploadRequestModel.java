package com.altium.deploy.web.model;

import org.springframework.web.multipart.MultipartFile;


public class UploadRequestModel {

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
