package com.altium.deploy.web.response;

import java.util.List;
import java.util.Map;


public interface Response {

    public boolean getSuccess();
    public String getMessage();
    public List<Map<String,String>> getData();
    public void addData(Map<String,String> data);
    public void setErrorMessage(String message);

}
