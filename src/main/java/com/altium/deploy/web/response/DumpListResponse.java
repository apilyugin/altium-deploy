package com.altium.deploy.web.response;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DumpListResponse implements Response {

    private List<Map<String,String>> data = new ArrayList<Map<String, String>>();
    private String message;
    private boolean success = true;

    public void addData(Map<String,String> data){
        this.data.add(data);
    }

    @Override
    public boolean getSuccess() {
        return success;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public List<Map<String, String>> getData() {
        return data;
    }

    @Override
    public void setErrorMessage(String message) {
        this.message = message;
        this.success = false;
    }
}
