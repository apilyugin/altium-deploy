package com.altium.deploy.web.response;

import java.util.List;
import java.util.Map;

/**
 * Created by alex on 12/25/13.
 */
public class ImportResponse implements Response {

    private String message;

    @Override
    public boolean getSuccess() {
        return false;
    }

    @Override
    public String getMessage() {
        return null;
    }

    @Override
    public List<Map<String, String>> getData() {
        return null;
    }

    @Override
    public void addData(Map<String, String> data) {

    }

    @Override
    public void setErrorMessage(String message) {
        this.message = message;
    }
}
