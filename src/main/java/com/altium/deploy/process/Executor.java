package com.altium.deploy.process;


import com.altium.deploy.db.model.DbModel;

public class Executor implements Runnable{

    private iProcess process;
    private DbModel dbModel;

    public Executor(iProcess process, DbModel dbModel){
        this.process = process;
        this.dbModel = dbModel;
    }

    @Override
    public void run() {
        this.process.execute(dbModel);
    }
}
