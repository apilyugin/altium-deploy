package com.altium.deploy.process;


import com.altium.deploy.Config;
import com.altium.deploy.action.Import;
import com.altium.deploy.db.ImportService;
import com.altium.deploy.db.UploadService;
import com.altium.deploy.db.model.DbModel;
import com.altium.deploy.db.model.ImportModel;
import com.altium.deploy.db.model.UploadModel;
import com.altium.deploy.error.AltiumDBException;
import com.altium.deploy.helper.Misc;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.io.IOException;

public abstract class AbstractImport implements iProcess {


    protected ImportService importService;
    protected UploadService uploadService;

    protected Config config = Config.getInstance();

    protected File unzipDir;
    protected File dbDir;
    protected File themesDir;
    protected File modulesDir;

    public void preProcess(DbModel dbModel){

        ImportModel importModel = (ImportModel) dbModel;
        try{
            UploadModel uploadModel = this.uploadService.getById(importModel.getDeployId());

            String deployDir = config.getDeployDir();
            String formatterDateString = Misc.getFormatterDateString();

            unzipDir = new File(deployDir + "/" + formatterDateString);

            if(!unzipDir.exists()){
                unzipDir.mkdir();
            }

            ZipUtil.unpack(new File(deployDir + "/" + uploadModel.getFilename()), unzipDir);

            dbDir = new File(unzipDir.getPath() + "/db");
            themesDir = new File(unzipDir.getPath() + "/fs/" + Config.THEMES_DIRECTORY);
            modulesDir = new File(unzipDir.getPath() + "/fs/" + Config.MODULES_DIRECTORY);
        } catch (Exception e){

        }


    }

    @Override
    public void postProcess(DbModel model) throws Exception{
        ImportModel importModel = (ImportModel) model;
        FileUtils.copyDirectory(themesDir, new File(config.getAltiumDir() + "/" + Config.THEMES_PATH));
        FileUtils.copyDirectory(modulesDir, new File(config.getAltiumDir() + "/" + Config.MODULES_PATH));
        importService.updateStatus(importModel.getId(), ImportModel.STATUS_COMPLETED);
        for(File file : unzipDir.listFiles()){
            file.delete();
        }
        FileUtils.forceDelete(unzipDir);
    }


}
