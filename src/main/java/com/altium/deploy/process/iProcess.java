package com.altium.deploy.process;

import com.altium.deploy.db.model.DbModel;

public interface iProcess {

    void preProcess(DbModel dbModel);
    void postProcess(DbModel dbModel) throws Exception;
    public void execute(DbModel dbModel);
}
