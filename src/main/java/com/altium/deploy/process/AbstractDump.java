package com.altium.deploy.process;

import com.altium.deploy.Config;
import com.altium.deploy.db.DumpService;
import com.altium.deploy.db.model.DbModel;
import com.altium.deploy.db.model.DumpModel;
import org.apache.commons.io.FileUtils;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;

public abstract class AbstractDump implements iProcess {


    protected DumpService dumpService;

    protected Config config = Config.getInstance();

    protected File dumpDirectory;
    protected File dbDir;
    protected File themesDir;
    protected File modulesDir;

    @Override
    public void preProcess(DbModel dbModel){
        DumpModel dumpModel = (DumpModel) dbModel;
        dumpDirectory = new File(config.getBackupDir() + "/" + dumpModel.getFilename());
        if(!dumpDirectory.exists()){
            dumpDirectory.mkdir();
        }
        dbDir = new File(dumpDirectory.getPath() + "/db");
        themesDir = new File(dumpDirectory.getPath() + "/fs/" + Config.THEMES_DIRECTORY);
        modulesDir = new File(dumpDirectory.getPath() + "/fs/" + Config.MODULES_DIRECTORY);

        dbDir.mkdir();
        themesDir.mkdirs();
        modulesDir.mkdirs();
    }

    @Override
    public void postProcess(DbModel dbModel) throws Exception{
        DumpModel dumpModel = (DumpModel) dbModel;
        ZipUtil.pack(new File(dumpDirectory.getPath()), new File(config.getZipDir() + "/" + dumpModel.getFilename() + ".zip"));
        FileUtils.forceDelete(dumpDirectory);
        dumpService.updateDumpStatus(dumpModel.getId(),DumpModel.STATUS_COMPLETED);

    }


}
