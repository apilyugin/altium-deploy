package com.altium.deploy.process.runtime.mysql;

import com.altium.deploy.Config;
import com.altium.deploy.db.DumpService;
import com.altium.deploy.db.model.DbModel;
import com.altium.deploy.process.AbstractDump;

public class Dump extends AbstractDump {

    private Config config;
    public static String DUMP_FILE_NAME = "dump.sql";

    public Dump() {
        this.config = Config.getInstance();
    }

    @Override
    public void execute(DbModel dbModel) {
        try{
            preProcess(dbModel);
            String executeCmd = config.getMysqlPath() + "mysqldump" + " -u " + config.getDbUserName() + " -p" + config.getDbPassword() + " " + config.getDbName() + " -r " + dbDir.getPath() + "/" + DUMP_FILE_NAME;
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();
            postProcess(dbModel);
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public void setDumpService(DumpService dumpService){
        this.dumpService = dumpService;
    }
}
