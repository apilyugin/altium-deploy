package com.altium.deploy.process.runtime.mysql;


import com.altium.deploy.Config;
import com.altium.deploy.db.ImportService;
import com.altium.deploy.db.UploadService;
import com.altium.deploy.db.model.DbModel;
import com.altium.deploy.process.AbstractImport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Import extends AbstractImport {

    private Config config;

    public Import(){
        this.config = Config.getInstance();
    }

    @Override
    public void execute(DbModel dbModel) {
        try{
            preProcess(dbModel);
            String[] executeCmd = {"/bin/sh","-c",config.getMysqlPath() + "mysql -u " + config.getDbUserName() + " -p" + config.getDbPassword() + " " + config.getDbName() + " --default-character-set=utf8" + " < "+ dbDir.getPath() + "/" + Dump.DUMP_FILE_NAME};
            //String executeCmd = config.getMysqlPath() + "mysql -u " + config.getDbUserName() + " -p" + config.getDbPassword() + " --default-character-set=utf8 " + config.getDbName() + " < " + source;
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(runtimeProcess.getErrorStream()));
            String line = null;
            while ( (line = errorReader.readLine()) != null){
                System.out.println(line);
            }
            postProcess(dbModel);
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public void setImportService(ImportService importService){
        this.importService = importService;
    }

    public void setUploadService(UploadService uploadService){
        this.uploadService = uploadService;
    }
}
