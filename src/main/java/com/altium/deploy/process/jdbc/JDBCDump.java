package com.altium.deploy.process.jdbc;

import com.altium.deploy.Config;
import com.altium.deploy.action.MySQLDump;
import com.altium.deploy.db.DumpService;
import com.altium.deploy.db.model.DbModel;
import com.altium.deploy.process.AbstractDump;


public class JDBCDump extends AbstractDump {

    private MySQLDump mySQLDump;

    public JDBCDump() throws Exception{
        mySQLDump = new MySQLDump(Config.getInstance());
    }


    @Override
    public void execute(DbModel dbModel) {
        try{
            preProcess(dbModel);
            mySQLDump.connect();
            mySQLDump.dumpAllTables(dbDir.getPath());
            mySQLDump.disconnect();
            postProcess(dbModel);
        } catch (Exception e){
            try{
                postProcess(dbModel);
                mySQLDump.disconnect();
            } catch (Exception ex){

            }
        }
    }

    public void setDumpService(DumpService dumpService){
        this.dumpService = dumpService;
    }
}
