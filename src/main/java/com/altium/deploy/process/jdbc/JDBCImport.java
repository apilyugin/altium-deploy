package com.altium.deploy.process.jdbc;

import com.altium.deploy.Config;
import com.altium.deploy.action.MySQLDump;
import com.altium.deploy.action.MySQLImport;
import com.altium.deploy.db.ImportService;
import com.altium.deploy.db.UploadService;
import com.altium.deploy.db.model.DbModel;
import com.altium.deploy.db.model.UploadModel;
import com.altium.deploy.process.AbstractDump;
import com.altium.deploy.process.AbstractImport;

import java.io.File;


public class JDBCImport extends AbstractImport {

    private MySQLImport mySQLImport;

    public JDBCImport() throws Exception{
        mySQLImport = new MySQLImport(Config.getInstance());
    }


    @Override
    public void execute(DbModel dbModel) {
        try{
            preProcess(dbModel);
            mySQLImport.connect();
            for(File file : dbDir.listFiles()){
                mySQLImport.importSQLFile(file);
            }
            mySQLImport.disconnect();
            postProcess(dbModel);
        } catch (Exception e){
           try{
               mySQLImport.disconnect();
           } catch (Exception ex){

           }
        }

    }


    public void setImportService(ImportService importService){
        this.importService = importService;
    }

    public void setUploadService(UploadService uploadService){
        this.uploadService = uploadService;
    }
}
