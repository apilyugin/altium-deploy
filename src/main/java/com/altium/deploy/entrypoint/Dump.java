package com.altium.deploy.entrypoint;


import biz.umber.sqldump.SQLDump;
import com.altium.deploy.Config;
import com.altium.deploy.action.Backup;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.util.Date;

public class Dump {


    public static void main(String[] args){
        Config config = Config.getInstance();
        Backup backup = new Backup(config);
        //backup.backupTables();
        compressFiles(config);
        removeBackupFiles(config);
    }

    public static void compressFiles(Config config){
        String backupDir = config.getBackupDir();
        ZipUtil.pack(new File(backupDir),new File(config.getZipDir() + "/" + SQLDump.fullYearDateTime.format(new Date()) + ".zip"));
    }

    public static void removeBackupFiles(Config config){
        File backupDir = new File(config.getBackupDir());
        if(backupDir.isDirectory() && backupDir.length() > 0){
            for(File file : backupDir.listFiles()){
                file.delete();
            }
        }
    }

}
