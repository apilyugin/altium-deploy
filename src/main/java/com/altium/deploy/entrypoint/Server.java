package com.altium.deploy.entrypoint;

import com.altium.deploy.db.Manager;
import com.altium.deploy.Config;
import com.altium.deploy.error.ConfigException;
import com.altium.deploy.web.WebServer;
import org.apache.commons.cli.*;


public class Server {

    public static void main(String[] ars) throws Exception{
        CommandLine commandLine = getCommandLine(ars);
        WebServer webServer = new WebServer(commandLine);
        Manager.create();
        Config.loadConfig(commandLine);
        webServer.start();
        webServer.join();

    }

    protected static CommandLine getCommandLine(String[] args) throws ParseException,ConfigException{
        Option configOption = new Option("c","config",true,"Config");
        Option ideOption = new Option("i","ide",false,"Run from IDE");
        Option portOption = new Option("p","port",true,"Server port");
        Options posixOptions = new Options();
        posixOptions.addOption(ideOption);
        posixOptions.addOption(configOption);
        posixOptions.addOption(portOption);
        CommandLineParser parser = new PosixParser();
        return parser.parse(posixOptions, args);
    }

    protected static String getConfigPath(String[] args) throws ParseException,ConfigException {
        Option option = new Option("c","config",true,"Config");
        Options posixOptions = new Options();
        posixOptions.addOption(option);
        CommandLineParser parser = new PosixParser();
        CommandLine commandLine = parser.parse(posixOptions, args);
        if(!commandLine.hasOption("c")){
            throw new ConfigException("Invalid start params");
        }

        return commandLine.getOptionValue("c");
    }

}
