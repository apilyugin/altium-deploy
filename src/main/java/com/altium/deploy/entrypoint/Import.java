package com.altium.deploy.entrypoint;



import biz.umber.sqldump.SQLDump;
import com.altium.deploy.Config;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Import {

    protected static List<Exception> exceptionList = new ArrayList<Exception>();

    public static void main(String[] args){
        Config config = Config.getInstance();
        unzipArchive(config.getZipDir() + "/20131223-185007.zip",config);
        importSql(config);
        removeSqlFiles(config);
    }

    public static void unzipArchive(String path,Config config){
        ZipUtil.unpack(new File(path),new File(config.getDeployDir()));

    }

    public static void importSql(Config config){
        try {
            PrintWriter logPrintStream = new PrintWriter(new File(config.getLogDir() + "/" + SQLDump.fullYearDateTime.format(new Date()) + "-execute.log"));
            PrintWriter errorPrintStream = new PrintWriter(new File(config.getLogDir() + "/" + SQLDump.fullYearDateTime.format(new Date()) + "-error.log"));
            com.altium.deploy.action.Import dbImport = new com.altium.deploy.action.Import(config,logPrintStream,errorPrintStream,false,false);
            File deployDir = new File(config.getDeployDir());
            if(deployDir.isDirectory() && (deployDir.length() > 0)){
                for(File file : deployDir.listFiles()){
                    dbImport.runScript(new FileReader(file));
                }
            }
        } catch (Exception e){
            exceptionList.add(e);
        }

    }

    public static void removeSqlFiles(Config config){
        File deployDir = new File(config.getDeployDir());
        if(deployDir.isDirectory() && (deployDir.length() > 0)){
            for(File file : deployDir.listFiles()){
                file.delete();
            }
        }
    }
}
