package com.altium.deploy.error;


public class AltiumDBException extends Exception {

    public AltiumDBException(String message){
        super(message);
    }

}
