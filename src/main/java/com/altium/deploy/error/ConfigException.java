package com.altium.deploy.error;

/**
 * Created by alex on 12/26/13.
 */
public class ConfigException extends Exception {
    public ConfigException(String message){
        super(message);
    }
}
