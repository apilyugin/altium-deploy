package com.altium.deploy.action;

import biz.umber.sqldump.DBStructureTree;
import biz.umber.sqldump.SQLDump;
import com.altium.deploy.Config;

import java.io.File;
import java.io.PrintStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Backup {


    private Connection connection;
    private Config config;
    private List<Exception> errorList = new ArrayList<Exception>();
    public static final Logger log = Logger.getLogger(Backup.class.getName());
    private static final Pattern crPattern = Pattern.compile("\r");
    private static final Pattern lfPattern = Pattern.compile("\n");
    private static final Pattern quotePattern = Pattern.compile("'");
    private static final Pattern cacheTable = Pattern.compile("cache");


    public Backup(Config config) {
        this.config = config;
    }

    public void backupTables(String path) {
        try {
            connect();
            String[] tableNames = getTableNames();
            for (String tableName : tableNames) {
                File file = new File(path + "/" + tableName + ".sql");
                PrintStream printStream = new PrintStream(file);
                dumpTable(tableName, printStream);
                printStream.close();
            }
            connection.close();
        } catch (Exception e) {
            errorList.add(e);
        }
    }

    public void dumpTable(String tableName, PrintStream out)
            throws SQLException {
        if (connection == null) {
            return;
        }

        String tableCreateSyntax = getTableCreateInfo(connection, tableName);
        writeOutput(out, "# drop table " + tableName);
        writeOutput(out, "DROP TABLE IF EXISTS " + tableName);
        writeOutput(out, "# create table " + tableName);
        writeOutput(out, tableCreateSyntax);
        writeOutput(out, "");
        writeOutput(out, "");


        String query = "";
        query = "select * from " + tableName;
        Statement stmt = connection.createStatement();
        stmt.setFetchSize(50);
        ResultSet rs = stmt.executeQuery(query);

        // only if we have data, start output
        if (rs.next()) {
            writeOutput(out, "# start dumping data for " + tableName);
            writeOutput(out, "/*!40000 ALTER TABLE " + tableName + " DISABLE KEYS */");
            writeOutput(out, "LOCK TABLES " + tableName + " WRITE");


            outputExtendedInsertQuery(rs, tableName, out);

            writeOutput(out, "UNLOCK TABLES");
            writeOutput(out, "/*!40000 ALTER TABLE " + tableName + " ENABLE KEYS */");

            writeOutput(out, "# done dumping data for " + tableName);
            writeOutput(out, "");
            writeOutput(out, "");
        }//end of rs.next()
        rs.close();
        stmt.close();

    }

    public void writeOutput(PrintStream out, String queryParam) {
        String query = queryParam;

        if (query == null) {
            query = "";
        }

        // add a semi colon for non-comment and non-blank lines
        if ((query.length() > 0) && !query.startsWith("#")) {
            query += ';';
        }

        if (out != null) {
            out.println(query);
        }

        if (log != null) {
            log.fine(query);
        }
    }


    public void outputExtendedInsertQuery(ResultSet rs, String tableName,
                                          PrintStream out) throws SQLException {

        String[] fieldNames = null;
        String fields = "";

        if (fieldNames == null) {
            fieldNames = getColumnNames(rs);
            fields = joinFieldNames(fieldNames);
        }

        StringBuffer query = new StringBuffer();

        // can't go over 1000 records in extended inserts
        // or 64k length of string
        int recCount = 0;
        if (rs.isBeforeFirst()) {
            rs.next();
        }

        if (!rs.isAfterLast()) {
            do {
                if (recCount == 0) {
                    query.append("insert into ").append(tableName).append(" (")
                            .append(fields).append(") values ");
                } else {
                    query.append(" , ");
                }
                recCount++;
                makeInsertQueryString(rs, tableName, fieldNames, query);

                // if this is not in Extended insert mode
                // or we hit 1000 records
                // then we need to finish this statement

                writeOutput(out, "# writing query for " + recCount + " records (" + query.length() + " chars)");
                writeOutput(out, query.toString());
                query.setLength(0);
                recCount = 0;

            } while (rs.next());


            writeOutput(out, "# writing query for " + recCount + " records (" + query.length() + " chars)");
            writeOutput(out, query.toString());

        }
    }

    public void makeInsertQueryString(ResultSet rs, String tableName,
                                      String[] fieldNameList, StringBuffer query) throws SQLException {
        String[] fieldNames = fieldNameList;

        if ((fieldNames == null) || (fieldNames.length == 0)) {
            fieldNames = getColumnNames(rs);
        }

        // make a comma separated string for field names
        StringBuffer values = new StringBuffer();
        boolean needComma = false;
        int colCount = fieldNames.length;
        for (int i = 0; i < colCount; i++) {
            String data = rs.getString(fieldNames[i]);

            if (needComma) {
                values.append(",");
            }

            if (data != null) {
                // fix the single quote handling
                data = convertString(data);
                //replace ; with , from the data.
                data = replaceSemicolonWithComma(data);
                values.append("'").append(data).append("'");
                needComma = true;
            } else {
                values.append("NULL");
                needComma = true;
            }
        }
        query.append("(").append(values).append(")");
    }

    public String replaceSemicolonWithComma(String data) {
        char oldChar = ';';
        char newChar = ',';
        return data.replace(oldChar, newChar);
    }

    public String[] getColumnNames(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int colCount = rsmd.getColumnCount();
        String[] retval = new String[colCount];

        for (int i = 0; i < colCount; i++) {
            retval[i] = rsmd.getColumnName(i + 1);
        }

        return retval;
    }

    public String getTableCreateInfo(Connection connection, String tableName)
            throws SQLException {
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SHOW CREATE TABLE " + tableName);

        String query = null;
        if (rs.next()) {
            // second column contains the string we need
            query = rs.getString(2);
        }
        return query;
    }

    String joinFieldNames(String[] strArr) {
        StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < strArr.length; i++) {
            if (i > 0) {
                buffer.append(",");
            }
            buffer.append("`" + strArr[i] + "`");
        }

        return buffer.toString();
    }

    String convertString(String dataParam) {
        String data = dataParam;
        data = quotePattern.matcher(data).replaceAll("\"");
        // remove all carriage returns
        Matcher m = crPattern.matcher(data);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "");
            sb.append("\\r");
        }
        m.appendTail(sb);
        data = sb.toString();

        // remove all new lines
        m = lfPattern.matcher(data);
        sb.setLength(0);
        while (m.find()) {
            m.appendReplacement(sb, "");
            sb.append("\\n");
        }
        m.appendTail(sb);
        data = sb.toString();
        return data;
    }

    protected String[] getTableNames() throws SQLException {
        return DBStructureTree.getOrderedTableNames(connection);
    }

    protected void connect() throws SQLException, ClassNotFoundException {
        Class.forName(SQLDump.DRIVER);
        connection = DriverManager.getConnection(config.getDbUrl(), config.getDbUserName(), config.getDbPassword());
    }

}
