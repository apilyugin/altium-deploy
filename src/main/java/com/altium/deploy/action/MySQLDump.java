/**
 * Copyright (c) 2007, Wave2 Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * Neither the name of Wave2 Limited nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.altium.deploy.action;

import biz.umber.sqldump.DBStructureTree;
import biz.umber.sqldump.SQLDump;
import com.altium.deploy.Config;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;

import java.io.*;
import java.sql.*;

/**
 *
 * @author Alan Snelson
 */
public class MySQLDump {


    private Config config;
    private Connection connection;
    private static Logger logger = Logger.getLogger(MySQLDump.class);



    public MySQLDump(Config config) throws SQLException, IOException{
        this.config = config;
        logger.addAppender(new FileAppender(new SimpleLayout(),config.getLogDir() + "/dump.log"));
    }


    public void connect() throws ClassNotFoundException, SQLException{
        Class.forName(SQLDump.DRIVER);
        connection = DriverManager.getConnection(config.getDbUrl(), config.getDbUserName(), config.getDbPassword());
    }

    public void disconnect() throws ClassNotFoundException, SQLException{
        if(connection !=null){
            connection.close();
        }
    }


    public String dumpCreateDatabase(String database) {
        String createDatabase = null;
        try{
            Statement s = connection.createStatement (ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            s.executeQuery ("SHOW CREATE DATABASE " + database);
            ResultSet rs = s.getResultSet ();
            while (rs.next ())
            {
                createDatabase = rs.getString("Create Database") + ";";
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return createDatabase;
    }


    public void dumpAllTables(String path) throws SQLException, IOException{
        for(String tableName : getTableNames()){
            logger.info("Dumping table: " + tableName);
            File tableSql = new File(path + "/" + tableName + ".sql");
            if(tableSql.exists()){
                tableSql.createNewFile();
            }
            BufferedWriter tableBufferedWriter = new BufferedWriter(new FileWriter(tableSql));
            dumpCreateTable(tableBufferedWriter, tableName);
            dumpTable(tableBufferedWriter,tableName);
            tableBufferedWriter.close();
            logger.info("Dumping table: " + tableName + " completed");
        }
    }

    public void dumpCreateTable(BufferedWriter out, String table) {
        String createTable = null;
        try{
            out.write("SET SQL_MODE=\"NO_AUTO_VALUE_ON_ZERO\";\n");
            out.write("SET time_zone=\"+00:00\";\n\n");
            out.write("DROP TABLE IF EXISTS " + table + ";\n\n\n");
            out.write("# create table " + table + ";\n");
            out.write("\n\n--\n-- Table structure for table `" + table + "`\n--\n\n");
            Statement s = connection.createStatement (ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            s.executeQuery ("SHOW CREATE TABLE " + table);
            ResultSet rs = s.getResultSet ();
            while (rs.next ())
            {
                createTable = rs.getString("Create Table") + ";";
            }
            out.write(createTable + "\n");
        } catch (SQLException e) {

        } catch(IOException e){
            System.err.println (e.getMessage());
        }
    }

    public void dumpTable(BufferedWriter out, String table){
        try{
            Statement s = connection.createStatement (ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            out.write("\n\n--\n-- Dumping data for table `" + table + "`\n--\n\n");
            s.executeQuery ("SELECT /*!40001 SQL_NO_CACHE */ * FROM " + table);
            ResultSet rs = s.getResultSet ();
            ResultSetMetaData rsMetaData = rs.getMetaData();
            int columnCount = rsMetaData.getColumnCount();
            String prefix = new String("INSERT INTO " + table + " (");
            for (int i = 1; i <= columnCount; i++) {
                if (i == columnCount){
                    prefix += rsMetaData.getColumnName(i) + ") VALUES(";
                }else{
                    prefix += rsMetaData.getColumnName(i) + ",";
                }
            }
            String postfix;
            int count = 0;
            while (rs.next ())
            {

                postfix = "";
                for (int i = 1; i <= columnCount; i++) {
                    if (i == columnCount){
                        System.err.println(rs.getMetaData().getColumnClassName(i));
                        postfix += "'" + rs.getString(i) + "');\n";
                    }else{

                        System.err.println(rs.getMetaData().getColumnTypeName(i));
                        if (rs.getMetaData().getColumnTypeName(i).equalsIgnoreCase("LONGBLOB")){
                            try{
                                postfix += "'" + escapeString(rs.getBytes(i)).toString() + "',";
                            }catch (Exception e){
                                postfix += "NULL,";
                            }
                        }else{
                            try{
                                postfix += "'" + rs.getString(i).replaceAll("\n","\\\\n").replaceAll("'","\\\\'") + "',";
                            }catch (Exception e){
                                postfix += "NULL,";
                            }
                        }   }
                }
                out.write(prefix + postfix + "\n");
                ++count;
            }
            rs.close ();
            s.close();
        }catch(IOException e){
            System.err.println (e.getMessage());
        }catch(SQLException e){
            System.err.println (e.getMessage());
        }
    }

    protected String[] getTableNames() throws SQLException {
        return DBStructureTree.getOrderedTableNames(connection);
    }

    /**
     * Escape string ready for insert via mysql client
     *
     * @param  bIn       String to be escaped passed in as byte array
     * @return bOut      MySQL compatible insert ready ByteArrayOutputStream
     */
    private ByteArrayOutputStream escapeString(byte[] bIn){
        int numBytes = bIn.length;
        ByteArrayOutputStream bOut = new ByteArrayOutputStream(numBytes+ 2);
        for (int i = 0; i < numBytes; ++i) {
            byte b = bIn[i];

            switch (b) {
                case 0: /* Must be escaped for 'mysql' */
                    bOut.write('\\');
                    bOut.write('0');
                    break;

                case '\n': /* Must be escaped for logs */
                    bOut.write('\\');
                    bOut.write('n');
                    break;

                case '\r':
                    bOut.write('\\');
                    bOut.write('r');
                    break;

                case '\\':
                    bOut.write('\\');
                    bOut.write('\\');

                    break;

                case '\'':
                    bOut.write('\\');
                    bOut.write('\'');

                    break;

                case '"': /* Better safe than sorry */
                    bOut.write('\\');
                    bOut.write('"');
                    break;

                case '\032': /* This gives problems on Win32 */
                    bOut.write('\\');
                    bOut.write('Z');
                    break;

                default:
                    bOut.write(b);
            }
        }
        return bOut;
    }



    public int cleanup(){
        try
        {
            connection.close ();
        }
        catch (Exception e) { /* ignore close errors */ }
        return 1;
    }

}
