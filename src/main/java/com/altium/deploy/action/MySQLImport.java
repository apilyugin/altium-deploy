package com.altium.deploy.action;

import biz.umber.sqldump.SQLDump;
import com.altium.deploy.Config;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;

import java.io.*;
import java.sql.*;


public class MySQLImport {


    private Connection connection;
    private Config config;
    private static Logger logger = Logger.getLogger(MySQLImport.class);
    private ScriptRunner scriptRunner;

    public MySQLImport(Config config) throws IOException{
        this.config = config;
        //logger.addAppender(new FileAppender(new SimpleLayout(),config.getLogDir() + "/import.log"));
    }

    public void importSQLFile(File file){
        try{
            System.out.println(file.getName());
            scriptRunner.runScript(new FileReader(file));
        } catch (FileNotFoundException e){
            logger.error(e);
        }
    }



    public void connect() throws SQLException, ClassNotFoundException {
        Class.forName(SQLDump.DRIVER);
        connection = DriverManager.getConnection(config.getDbUrl(), config.getDbUserName(), config.getDbPassword());
        scriptRunner = new ScriptRunner(connection);
        scriptRunner.setStopOnError(false);
        try {
            scriptRunner.setLogWriter(new PrintWriter(new File(config.getLogDir() + "/import.log")));
        } catch (Exception e){
            logger.error(e);
        }

    }

    public void disconnect(){
        try{
            if(connection != null){
                connection.close();
            }
        } catch (SQLException e){
            logger.error(e);
        }

    }
}
