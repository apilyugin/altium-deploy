package com.altium.deploy.db;

import com.altium.deploy.db.model.DbModel;
import com.altium.deploy.db.model.ServerModel;
import com.altium.deploy.error.AltiumDBException;

import java.sql.*;
import java.util.UUID;


public class ServerService {

    private Connection connection;

    public ServerService() throws AltiumDBException{
        this.connection = Manager.getConnection();
    }

    synchronized public ResultSet getList() throws AltiumDBException{
        try {
            Statement find = connection.createStatement();
            return find.executeQuery("select * from " + Manager.serverTable);
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    synchronized public DbModel add(ServerModel serverModel) throws AltiumDBException{
        try {
            PreparedStatement addDumpStatement = connection.prepareStatement("insert into " + Manager.serverTable + " values(?,?,?,?)");
            String id = getId();
            addDumpStatement.setString(1, id);
            addDumpStatement.setString(2, serverModel.getName());
            addDumpStatement.setString(3, serverModel.getUrl());
            addDumpStatement.setString(4,serverModel.getPort());
            addDumpStatement.executeUpdate();
            connection.commit();
            return this.getById(id);
        } catch (Exception e){
            try {
                connection.rollback();
            } catch (SQLException ex){
                throw new AltiumDBException(e.getMessage());
            }
            throw new AltiumDBException(e.getMessage());
        }

    }

    synchronized public DbModel update(ServerModel serverModel) throws AltiumDBException{
        try{
            PreparedStatement preparedStatement = connection.prepareStatement("update "  + Manager.serverTable + " set name=?, url=?, port=? where id=?");
            preparedStatement.setString(1,serverModel.getName());
            preparedStatement.setString(2,serverModel.getUrl());
            preparedStatement.setString(3,serverModel.getPort());
            preparedStatement.setString(4,serverModel.getId());
            preparedStatement.executeUpdate();
            preparedStatement.execute();
            connection.commit();
            return serverModel;
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    synchronized public void delete(String id) throws AltiumDBException{
        try{
            PreparedStatement preparedStatement = connection.prepareStatement("delete from " + Manager.serverTable + " where id=?");
            preparedStatement.setString(1,id);
            preparedStatement.executeUpdate();
            preparedStatement.execute();
            connection.commit();
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    synchronized public DbModel getById(String id) throws AltiumDBException{
        try {
            ServerModel result = new ServerModel();
            Statement select = connection.createStatement();
            ResultSet resultSet = select.executeQuery("select * from " + Manager.serverTable + " WHERE id='" + id + "'");
            if(resultSet.next()){
                result.importData(resultSet);
            } else {
                throw new AltiumDBException("Invalid id");
            }
            return result;
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    private String getId(){
        return UUID.randomUUID().toString();
    }





}
