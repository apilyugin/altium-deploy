package com.altium.deploy.db;

import com.altium.deploy.db.model.UploadModel;
import com.altium.deploy.error.AltiumDBException;

import java.sql.*;
import java.util.Date;
import java.util.UUID;


public class UploadService {

    private Connection connection;

    public UploadService() throws AltiumDBException{
        this.connection = Manager.getConnection();
    }

    synchronized public ResultSet getList() throws AltiumDBException{
        try {
            Statement find = connection.createStatement();
            return find.executeQuery("select * from " + Manager.uploadTable + " order by created_on desc");
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    synchronized public UploadModel addUpload(String filename) throws AltiumDBException{
        try {
            String id = getId();
            PreparedStatement addUploadStatement = connection.prepareStatement("insert into " + Manager.uploadTable + " values(?,?,?)");
            addUploadStatement.setString(1, id);
            addUploadStatement.setString(2, filename);
            addUploadStatement.setTimestamp(3,new Timestamp(new Date().getTime()));
            addUploadStatement.executeUpdate();
            connection.commit();
            return this.getById(id);
        } catch (Exception e){
            try {
                connection.rollback();
            } catch (SQLException ex){
                throw new AltiumDBException(e.getMessage());
            }
            throw new AltiumDBException(e.getMessage());
        }

    }

    synchronized public UploadModel getById(String id) throws AltiumDBException{
        try {
            UploadModel result = new UploadModel();
            Statement select = connection.createStatement();
            ResultSet resultSet = select.executeQuery("select * from " + Manager.uploadTable + " WHERE id='" + id + "'");
            if(resultSet.next()){
                result.importData(resultSet);
            }
            return result;
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    private String getId(){
        return UUID.randomUUID().toString();
    }





}
