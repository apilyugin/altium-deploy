package com.altium.deploy.db;

import com.altium.deploy.error.AltiumDBException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public abstract class AbstractTable {

    protected Connection connection;

    public ResultSet find(String table, String where, int offset, int count, String orderProperty, String orderDirection) throws AltiumDBException{
        try {
            Statement find = connection.createStatement();
            return find.executeQuery(buildSql(table,where,offset,count,orderProperty,orderDirection));
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    public AbstractModel save(){
        return null;
    }

    public AbstractModel update(){
        return null;
    }

    public void delete(){

    }

    protected String buildSql(String table, String where, int offset, int count, String orderProperty, String orderDirection){
        String sql = "SELECT * from " + table;
        if(where != null){
            sql += " WHERE " + where;
        }
        sql += " LIMIT " + offset + "," + count;
        if((orderProperty != null) && (orderDirection != null)){
            sql += " ORDER BY " + orderProperty + " " + orderDirection;
        }
        return sql;
    }



}
