package com.altium.deploy.db;

import com.altium.deploy.db.model.DumpModel;
import com.altium.deploy.error.AltiumDBException;


import java.sql.*;
import java.util.Date;
import java.util.UUID;


public class DumpService {

    private Connection connection;

    public DumpService() throws AltiumDBException{
        this.connection = Manager.getConnection();
    }

    synchronized public ResultSet getList() throws AltiumDBException{
        try {
            Statement find = connection.createStatement();
            return find.executeQuery("select * from " + Manager.dumpTable + " ORDER BY created_on DESC");
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    synchronized public DumpModel addDump(String filename) throws AltiumDBException{
        try {
            String id = getId();
            PreparedStatement addDumpStatement = connection.prepareStatement("insert into " + Manager.dumpTable + " values(?,?,?,?)");
            addDumpStatement.setString(1, id);
            addDumpStatement.setString(2, filename);
            addDumpStatement.setString(3, "in progress");
            addDumpStatement.setTimestamp(4,new Timestamp(new Date().getTime()));
            addDumpStatement.executeUpdate();
            connection.commit();
            return this.getById(id);
        } catch (Exception e){
            try {
                connection.rollback();
            } catch (SQLException ex){
                throw new AltiumDBException(e.getMessage());
            }
            throw new AltiumDBException(e.getMessage());
        }

    }

    synchronized public DumpModel updateDumpStatus(String id, String status) throws AltiumDBException{
        try{
            DumpModel dumpModel = this.getById(id);
            Statement statement = connection.createStatement();
            statement.execute("update " + Manager.dumpTable + " set status='" + status + "' where id='" + id + "'");
            connection.commit();
            dumpModel.setStatus(status);
            return dumpModel;
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    synchronized public DumpModel getById(String id) throws AltiumDBException{
        try {
            DumpModel result = new DumpModel();
            Statement select = connection.createStatement();
            ResultSet resultSet = select.executeQuery("select * from " + Manager.dumpTable + " WHERE id='" + id + "'");
            if(resultSet.next()){
                result.importData(resultSet);
            } else {
                throw new AltiumDBException("Invalid id");
            }
            return result;
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    private String getId(){
        return UUID.randomUUID().toString();
    }





}
