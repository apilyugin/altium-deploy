package com.altium.deploy.db;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AbstractModel {

    protected Map<String,Object> values = new HashMap<String, Object>();
    protected List<String> fields = new ArrayList<String>();
    protected Map<String,Class> fieldTypes = new HashMap<String, Class>();
    protected Map<String,Object> changes = new HashMap<String, Object>();


    public void importData(ResultSet rs){
        for(String field : fields){
            try{
                int columnNumber = rs.findColumn(field);
                Object fieldVal = rs.getObject(columnNumber);
                values.put(field, fieldVal);
            } catch (Exception e){

            }

        }
    }

    public void set(String field, Object value){
        if(fields.contains(field)){
            values.put(field, value);
            changes.put(field,value);
        }
    }

    public Object get(String field){
        Object result = null;
        if(fields.contains(field) && values.containsKey(field)){
            result = values.get(field);
        }
        return result;
    }

}
