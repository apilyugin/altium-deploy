package com.altium.deploy.db;

import com.altium.deploy.db.model.ImportModel;
import com.altium.deploy.db.model.UploadModel;
import com.altium.deploy.Config;
import com.altium.deploy.error.AltiumDBException;

import java.sql.*;
import java.util.*;
import java.util.Date;


public class ImportService {


    private UploadService uploadService;
    private Config config;

    private Connection connection;

    public ImportService() throws AltiumDBException{
        this.connection = Manager.getConnection();
        config = Config.getInstance();
    }

    synchronized public ResultSet getList() throws AltiumDBException{
        try {
            Statement find = connection.createStatement();
            return find.executeQuery("select * from " + Manager.importTable + " order by started_on desc");
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    synchronized public ImportModel add(String uploadId) throws AltiumDBException{
        try{
            UploadModel uploadModel = uploadService.getById(uploadId);
            if(uploadModel.getId() == null){
                throw new AltiumDBException("Invalid id");
            }
            String id = this.getId();
            PreparedStatement addImportStatement = connection.prepareStatement("insert into " + Manager.importTable + " values(?,?,?,?,null)");
            addImportStatement.setString(1, id);
            addImportStatement.setString(2, uploadId);
            addImportStatement.setString(3, "in progress");
            addImportStatement.setTimestamp(4, new Timestamp(new java.util.Date().getTime()));
            addImportStatement.executeUpdate();
            connection.commit();
            return this.getById(id);

        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    synchronized public ImportModel updateStatus(String id, String status) throws AltiumDBException{
        try {
            ImportModel importModel = this.getById(id);
            if(importModel.getId() == null){
                throw new AltiumDBException("Invalid id");
            }
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE " + Manager.importTable + " set status=?, finished_on=? WHERE id=?");
            preparedStatement.setString(1,status);
            preparedStatement.setTimestamp(2,new Timestamp(new Date().getTime()));
            preparedStatement.setString(3,id);
            preparedStatement.executeUpdate();
            preparedStatement.execute();
            connection.commit();
            return getById(id);
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    synchronized public ImportModel getById(String id) throws AltiumDBException {
        try {
            ImportModel result = new ImportModel();
            Statement select = connection.createStatement();
            ResultSet resultSet = select.executeQuery("select * from " + Manager.importTable + " WHERE id='" + id + "'");
            if(resultSet.next()){
                result.importData(resultSet);
            } else {
                throw new AltiumDBException("Invalid id");
            }
            return result;
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    private String getId(){
        return UUID.randomUUID().toString();
    }

    public void setUploadService(UploadService uploadService){
        this.uploadService = uploadService;
    }
}
