package com.altium.deploy.db.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by alex on 12/26/13.
 */
public interface DbModel {

    public void importData (ResultSet rs) throws SQLException;

    public Map<String,String> getData();

}
