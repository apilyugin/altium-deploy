package com.altium.deploy.db.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class UploadModel implements DbModel{

    private String id;
    private String filename;
    private Date createdOn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public void importData(ResultSet rs) throws SQLException{
        this.id = rs.getString(1);
        this.filename = rs.getString(2);
        this.createdOn = new Date(rs.getTimestamp(3).getTime());
    }

    @Override
    public Map<String, String> getData() {
        Map<String,String> data = new HashMap<String, String>();
        data.put("id",id);
        data.put("filename",filename);
        data.put("createdOn",createdOn.toString());
        return data;
    }
}
