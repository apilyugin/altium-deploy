package com.altium.deploy.db.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class DumpModel implements DbModel {


    public static String STATUS_IN_PROGRESS = "in progress";
    public static String STATUS_COMPLETED = "completed";
    public static String STATUS_FAILURE = "failure";
    public static String DUMP_EXTENSION = ".zip";


    private String id;
    private String filename;
    private String status = STATUS_IN_PROGRESS;
    private Date createdOn;

    public enum STATUSES{

    };



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public void importData(ResultSet rs) throws SQLException {
        this.id = rs.getString(1);
        this.filename = rs.getString(2);
        this.status = rs.getString(3);
        this.createdOn = new Date(rs.getTimestamp(4).getTime());
    }

    @Override
    public Map<String, String> getData() {
        Map<String,String> data = new HashMap<String, String>();
        data.put("id",id);
        data.put("filename",filename);
        data.put("status",status);
        data.put("created_on",createdOn.toString());
        return data;
    }
}
