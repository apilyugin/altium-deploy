package com.altium.deploy.db.model;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ServerModel implements DbModel {

    private String id;
    private String name;
    private String url;
    private String port;

    public ServerModel(){

    }

    public ServerModel(String name, String url, String port){
        this.name = name;
        this.url = url;
        this.port = port;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    @Override
    public void importData(ResultSet rs) throws SQLException {
        this.id = rs.getString(1);
        this.name = rs.getString(2);
        this.url = rs.getString(3);
        this.port = rs.getString(4);
    }

    @Override
    public Map<String, String> getData() {
        Map<String,String> data = new HashMap<String, String>();
        data.put("id",id);
        data.put("name",name);
        data.put("url",url);
        data.put("port",port);
        return data;
    }
}
