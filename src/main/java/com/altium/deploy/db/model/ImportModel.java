package com.altium.deploy.db.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class ImportModel implements DbModel {

    public static String STATUS_IN_PROGRESS = "in progress";
    public static String STATUS_COMPLETED = "completed";
    public static String STATUS_FAILURE = "failure";

    private String id;
    private String deployId;
    private String status = STATUS_IN_PROGRESS;
    private Date startedOn;
    private Date finishedOn;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeployId() {
        return deployId;
    }

    public void setDeployId(String deployId) {
        this.deployId = deployId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(Date startedOn) {
        this.startedOn = startedOn;
    }

    public Date getFinishedOn() {
        return finishedOn;
    }

    public void setFinishedOn(Date finishedOn) {
        this.finishedOn = finishedOn;
    }

    @Override
    public void importData(ResultSet rs) throws SQLException {
        this.id = rs.getString(1);
        this.deployId = rs.getString(2);
        this.status = rs.getString(3);
        try{
            Timestamp timestamp = rs.getTimestamp(4);
            if(timestamp != null){
                this.startedOn = new Date(timestamp.getTime());
            }
        } catch (Exception e){
            //Nothing to do
        }
        try {
            Timestamp timestamp = rs.getTimestamp(5);
            if(timestamp != null){
                this.finishedOn = new Date(timestamp.getTime());
            }
        } catch (Exception e){
            //Nothing to do
        }

    }

    @Override
    public Map<String, String> getData() {
        Map<String,String> data = new HashMap<String, String>();
        data.put("id",id);
        data.put("deployId",deployId);
        data.put("status",status);
        if(startedOn != null){
            data.put("staredOn",startedOn.toString());
        }
        if(finishedOn != null){
            data.put("finishedOn",finishedOn.toString());
        }

        return data;
    }
}
