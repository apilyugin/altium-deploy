package com.altium.deploy.db;

import com.altium.deploy.error.AltiumDBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Manager {

    private static String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    public static String importTable = "import";
    public static String dumpTable = "dump";
    public static String uploadTable = "upload";
    public static String serverTable = "server";
    private static String dbUrl = "jdbc:derby:altiumDeploy;create=true";
    private static Connection connection;
    private static List<String> applicationTables = new ArrayList<String>(){{
        add(importTable);
        add(dumpTable);
        add(uploadTable);
        add(serverTable);
    }};
    private static Map<String,String> appTablesCreateSQL = new HashMap<String, String>(){{
        put(dumpTable,"CREATE TABLE " + dumpTable + "(id varchar(244) not null unique , filename varchar(255) not null, status varchar(50) with default 'in progress', created_on timestamp with default current_timestamp )");
        put(uploadTable,"CREATE TABLE " + uploadTable + "(id varchar(244) not null unique , filename varchar(255) not null, created_on timestamp with default current_timestamp )");
        put(importTable,"CREATE TABLE " + importTable + "(id varchar(244) unique , upload_id varchar(255), status varchar(50) with default 'in progress', started_on timestamp with default null, finished_on timestamp with default null)");
        put(serverTable,"CREATE TABLE " + serverTable + "(id varchar(244) unique , name varchar(255) not null, url varchar(255) not null, port varchar(5))");
    }};


    public static void create() throws AltiumDBException{
        try {
            createTables(getConnection());
        } catch (AltiumDBException e){
            throw e;
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    public static Connection getConnection() throws AltiumDBException{
        try {
            if(connection == null){
                loadDriver();
                connection = DriverManager.getConnection(dbUrl);
                connection.setAutoCommit(false);
            }
            return  connection;
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    private static void loadDriver() throws AltiumDBException{
        try {
            Class.forName(DRIVER).newInstance();
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }

    private static void createTables(Connection connection) throws AltiumDBException{
        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet tables = metaData.getTables(null, null, null, null);
            List<String> foundTables = new ArrayList<String>();
            while (tables.next()){
                String tableName = tables.getString("TABLE_NAME");
                if(appTablesCreateSQL.containsKey(tableName.toLowerCase())){
                    foundTables.add(tableName.toLowerCase());
                }
            }
            for(Map.Entry<String,String> entry : appTablesCreateSQL.entrySet()){
                if(foundTables.indexOf(entry.getKey()) == -1){
                    Statement statement = connection.createStatement();
                    statement.execute(entry.getValue());
                }
            }
        } catch (Exception e){
            throw new AltiumDBException(e.getMessage());
        }
    }


}
