package com.altium.deploy.scp;


import com.altium.deploy.Config;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.transport.TransportException;
import net.schmizz.sshj.userauth.UserAuthException;
import net.schmizz.sshj.xfer.FileSystemFile;

import java.io.File;
import java.io.IOException;

public class Upload {

    private SSHClient sshClient;
    private Config config;


    public Upload(Config config) throws TransportException, UserAuthException{
        this.config = config;
        sshClient = new SSHClient();
    }

    public void upload(File file) throws IOException{
        //sshClient.connect();
        //sshClient.loadKnownHosts(new File("/Users/alex/.ssh/knows_hosts"));
        sshClient.addHostKeyVerifier("c2:5d:e5:d0:f5:43:d8:52:73:22:fd:b6:7e:e3:ca:34");
        sshClient.connect(config.getScpHostName(),Integer.parseInt(config.getScpPort()));
        sshClient.authPassword(config.getScpUser(),config.getScpPassword());
        sshClient.newSCPFileTransfer().upload(new FileSystemFile(file), config.getScpTargetDir());
        sshClient.disconnect();
    }
}
